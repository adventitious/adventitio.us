import React, { useState } from "react";
import {
    useParams,
    useNavigate,
    Link
} from "react-router-dom";
import { gql } from "graphql-tag";
import { Button } from "@zendeskgarden/react-buttons";
import { Breadcrumb } from "@zendeskgarden/react-breadcrumbs";
import {
    Field,
    Label,
    Input,
    Textarea,
    Hint
} from "@zendeskgarden/react-forms";

import { useQuery, useMutation } from "../../../api/graphql/hooks.ts";


export const NewActivity = () => {

    const navigate = useNavigate();
    const { id } = useParams(); 
    const [ title, setTitle ] = useState("");
    const [ description, setDescription ] = useState("");

    const {
        mutate: createActivity,
        pending: pendingActivity
    } = useMutation(gql`
        mutation NewActivity(
            $title: String
            $description: String
        ) {
            createActivity(
                title: $title
                description: $description
            ) {
                id
            }
        }
    `, {});

    return (<>
        <section className="heading">
            <Breadcrumb>
                <Link to="/">Home</Link>
                <Link to="/activities">Activities</Link>
                <span>New Activity</span>
            </Breadcrumb>
            <h1>Create a Activity</h1>
        </section>
        <section>
            <Field>
                <Label>Title</Label>
                <Hint>Describe the activity as concisely as possible.</Hint>
                <Input 
                    onChange={(event: any) => {
                        setTitle(event.target.value);
                    }}
                />
            </Field>
            <Field>
                <Label>Description</Label>
                <Hint>Describe the activity in as much detail as possible.</Hint>
                <Textarea
                    isResizable
                    minRows={7}
                    maxRows={17}
                    onChange={(event: any) => {
                        setDescription(event.target.value);
                    }}
                />
            </Field>
            <div className="actions">
                <div className="proceed">
                    <Button
                        isPrimary
                        onClick={async () => {
                            const { data } = await createActivity({ title, description });
                            navigate(`/activities/${data.createActivity.id}`);
                        }}
                    >
                        Submit
                    </Button>
                </div>
            </div>
        </section>
    </>);
};
