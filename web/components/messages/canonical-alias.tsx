import React from "react";

export const CanonicalAlias = ({ content }) => {
    return (
        <span className="system-message">
            Set Room Alias to {content.alias}
        </span>
    );
};

