import React, {
    useEffect,
    useState,
    useCallback,
    useContext,
    createContext
} from "react";
import { useNavigate } from "react-router-dom";
import { Button } from "@zendeskgarden/react-buttons";
import {
    Field,
    Label,
    Input,
    Textarea,
    Hint
} from "@zendeskgarden/react-forms";
import {
    Alert,
    Title
} from "@zendeskgarden/react-notifications";

import { env } from "../../env.ts";

import { AuthContext } from "../context/auth.ts";
//const matrixSdk = env.RUNTIME === "browser" ? await import("matrix-js-sdk") : null;
//const matrixClient = matrixSdk?.default?.createClient?.("http://localhost:8008");



export const Auth = () => {

    const navigate = useNavigate();

    const {
        username,
        accessToken,
        setGuest,
        setUsername,
        setAccessToken,
        setHomeServer,
        setDeviceId
    } = useContext(AuthContext);

    const [ usernameFieldValue, setUsernameFieldValue ] = useState();
    const [ passwordFieldValue, setPasswordFieldValue ] = useState();
    const [ instructions, setInstructions ] = useState(null);

    useEffect(() => {
        console.log("authenticating");
        //matrixClient.publicRooms((err, data) => {
        //    console.log("Public Rooms: %s", JSON.stringify(data));
        //});
    }, []);

    const registerCallback = useCallback(async () => {
        const authResponse = await fetch("/_matrix/client/v3/register", {
            method: "POST",
            body: JSON.stringify({
                auth: { type: "m.login.dummy" },
                username: usernameFieldValue,
                password: passwordFieldValue,
                guest_access_token: accessToken
            })
        });

        const responseObject = await authResponse.json();

        if (responseObject.errcode === "M_WEAK_PASSWORD") {
            setInstructions("The password isnt strong enough. Choose a longer or less commonly used password")
            return;
        }

        if (responseObject.errcode === "M_USER_IN_USE") {
            const authResponse = await fetch("/_matrix/client/v3/login", {
                method: "POST",
                body: JSON.stringify({
                    type: "m.login.password",
                    identifier: {
                        type: "m.id.user",
                        user: usernameFieldValue
                    },
                    password: passwordFieldValue
                })
            });
            const responseObject = await authResponse.json();

            if (responseObject.errcode === "M_FORBIDDEN") {
                setInstructions("The username or password was incorrect.")
                return;
            }
            if (responseObject.errcode === "M_WEAK_PASSWORD") {
                setInstructions("The password isnt strong enough. Choose a longer or less commonly used password")
                return;
            }

            const {
                user_id,
                access_token,
                well_known,
                device_id
            } = responseObject;

            setGuest(false);
            setUsername(user_id);
            setAccessToken(access_token);
            setHomeServer(well_known);
            setDeviceId(device_id);

            navigate("/");

            return;
            
        }
    
        if (responseObject.errcode) {
            console.log({ err: responseObject.errcode });
            return;
        }

        const {
            user_id,
            access_token,
            home_server,
            device_id
        } = responseObject;

        setGuest(false);
        setUsername(user_id);
        setAccessToken(access_token);
        setHomeServer(home_server);
        setDeviceId(device_id);

        navigate("/");

    }, [
        usernameFieldValue,
        passwordFieldValue,
        username,
        setUsername,
        setAccessToken,
        setHomeServer,
        setDeviceId
    ]);

    return (<>
        <section className="heading">
            <h1>Authenticate</h1>
        </section>
        <section>
            <p>Enter a username and password to authenticate with. If the account does not exist it will automatically be created for you.</p>
            {instructions
                ? <Alert type="error">
                    <Title>There was a problem authenticating</Title>
                    <p>{instructions}</p>
                </Alert>
                : <></>
            }
            <Field>
                <Label>Username</Label>
                <Input 
                    onChange={(event: any) => {
                        setUsernameFieldValue(event.target.value);
                    }}
                />
            </Field>
            <Field>
                <Label>Password</Label>
                <Input 
                    type="password"
                    onChange={(event: any) => {
                        setPasswordFieldValue(event.target.value);
                    }}
                />
            </Field>
            <div className="actions">
                <div className="proceed">
                    <Button 
                        isPrimary
                        onClick={registerCallback}
                    >
                        Authenticate
                    </Button>
                </div>
            </div>
        </section>
    </>)


}

