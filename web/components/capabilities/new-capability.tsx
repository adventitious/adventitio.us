import React, { useState } from "react";
import {
    useParams,
    useNavigate,
    Link
} from "react-router-dom";
import { gql } from "graphql-tag";
import { Button } from "@zendeskgarden/react-buttons";
import { Breadcrumb } from "@zendeskgarden/react-breadcrumbs";
import {
    Field,
    Label,
    Input,
    Textarea,
    Hint
} from "@zendeskgarden/react-forms";

import { useQuery, useMutation } from "../../../api/graphql/hooks.ts";


export const NewCapability = () => {

    const navigate = useNavigate();
    const { id } = useParams(); 
    const [ title, setTitle ] = useState("");
    const [ description, setDescription ] = useState("");
    const [ contactInfo, setContactInfo ] = useState("");

    const {
        mutate: createCapability,
        pending: pendingCapability
    } = useMutation(gql`
        mutation NewCapability(
            $title: String
            $description: String
            $contactInfo: String
        ) {
            createCapability(
                title: $title
                description: $description
                contactInfo: $contactInfo
            ) {
                id
            }
        }
    `, {});

    return (<>
        <section className="heading">
            <Breadcrumb>
                <Link to="/">Home</Link>
                <Link to="/capabilities">Capabilities</Link>
                <span>New Capability</span>
            </Breadcrumb>
            <h1>Create a Capability</h1>
        </section>
        <section>
            <Field>
                <Label>Title</Label>
                <Hint>Describe the capability as concisely as possible.</Hint>
                <Input 
                    onChange={(event: any) => {
                        setTitle(event.target.value);
                    }}
                />
            </Field>
            <Field>
                <Label>Description</Label>
                <Hint>Describe the capability in as much detail as possible.</Hint>
                <Textarea
                    isResizable
                    minRows={7}
                    maxRows={17}
                    onChange={(event: any) => {
                        setDescription(event.target.value);
                    }}
                />
            </Field>
            <Field>
                <Label>Contact Info</Label>
                <Hint>Provide information about how best to make contact.</Hint>
                <Textarea
                    isResizable
                    minRows={2}
                    maxRows={5}
                    onChange={(event: any) => {
                        setContactInfo(event.target.value);
                    }}
                />
            </Field>
            <div className="actions">
                <div className="proceed">
                    <Button
                        isPrimary
                        onClick={async () => {
                            const { data } = await createCapability({ title, description, contactInfo });
                            navigate(`/capabilities/${data.createCapability.id}`);
                        }}
                    >
                        Submit
                    </Button>
                </div>
            </div>
        </section>
    </>);
};
