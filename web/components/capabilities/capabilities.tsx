import React from "react";
import { useNavigate, Link } from "react-router-dom";
import gql from "graphql-tag";
import { Button, ButtonGroup } from "@zendeskgarden/react-buttons";
import { Breadcrumb } from "@zendeskgarden/react-breadcrumbs";
import {
    Body,
    Cell,
    Head,
    HeaderCell,
    HeaderRow,
    Row,
    Table
} from '@zendeskgarden/react-tables';
import { Ellipsis } from '@zendeskgarden/react-typography';
import {
    PlusIcon,
    DashIcon
} from "@primer/octicons-react";

import { useQuery } from "../../../api/graphql/hooks.ts";

import { ListView } from "../ListView.tsx";


export const Capabilities = () => {

    const navigate = useNavigate();
        
    const { data, loading } = useQuery(gql`
        query Capabilities {
            capabilities: allCapabilities {
                id
                title
                description
                contactInfo
            }
        }
    `);

    return (<ListView
        loading={loading}
        heading={<>
            <Breadcrumb>
                <Link to="/">Home</Link>
                <span>Capabilities</span>
            </Breadcrumb>
            <h1>Capabilities</h1>
        </>}
        data={data?.capabilities}
        columns={[{
            displayName: "ID",
            render: ({ id, title }: any) => (
                <Link to={`/capabilities/${id}`}>
                    <Ellipsis style={{ width: 70 }}>{id}</Ellipsis>
                </Link>
            ),
            style: {
                fontWeight: "500",
                fontFamily: "Fira Mono"
            }
        }, {
            displayName: "Title",
            render: ({ id, title }: any) => (<Link to={`/capabilities/${id}`}>{title}</Link>),
        }, {
            displayName: "demand",
            render: ({ id, demand = 0 }: any) => {
                return (<div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        gap: "0.5rem"
                    }}
                >
                    <Button size="small"><DashIcon /></Button>
                    <Button size="small"><PlusIcon /></Button>
                    <span>{demand}</span>
                </div>);
            }
        }]}
        proceedActions={<>
            <Button
                isPrimary
                onClick={() => {
                    navigate("new")
                }}
            >
                Add a Capability
            </Button>
        </>}
    />);
};
