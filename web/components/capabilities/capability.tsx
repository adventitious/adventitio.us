import React, {
    useContext,
    useState,
    useEffect,
    useCallback
} from "react";
import { useParams, Link } from "react-router-dom";
import { gql } from "graphql-tag";
import { Skeleton } from "@zendeskgarden/react-loaders";
import { Textarea } from "@zendeskgarden/react-forms";
import { Button, IconButton } from "@zendeskgarden/react-buttons";
import { Breadcrumb } from "@zendeskgarden/react-breadcrumbs";
import { Ellipsis } from '@zendeskgarden/react-typography';
import {
    PlusIcon,
    DashIcon,
    PaperAirplaneIcon
} from "@primer/octicons-react";
import { nanoid } from "nanoid";

import { useQuery } from "../../../api/graphql/hooks.ts";

import { AuthContext } from "../../context/auth.ts";

import { Message } from "../messages.tsx";


export const Capability = () => {

    const { accessToken } = useContext(AuthContext);
    const { id } = useParams(); 
    const [ roomId, setRoomId ] = useState(null);
    const [ messages, setMessages ] = useState([]);
    const [ draftMessage, setDraftMessage ] = useState("");
    const [ loadingOlderMessages, setLoadingOlderMessages ] = useState(null);
    const [ oldestMessageCursor, setOldestMessageCursor ] = useState(null);
    const [ latestMessageCursor, setLatestMessageCursor ] = useState(null);

    const { data, loading } = useQuery(gql`
        query Capability($id: ID!) {
            capability: findCapability(id: $id) {
                id
                title
                description
                contactInfo
                demand
            }
        }
    `, {
        variables: { id }
    });

    useEffect(() => {
        (async () => {
            const roomIDResponse = await fetch(`/_matrix/client/v3/directory/room/%23capability_${id}:adventitio.us`, {
                method: "GET"
            });
            const { room_id: roomId } = await roomIDResponse.json();
            console.log({ roomId });
            setRoomId(roomId);
            const joinRoomResponse = await fetch(`/_matrix/client/v3/join/%23capability_${id}:adventitio.us?access_token=${accessToken}`, {
                method: "POST"
            });
            await joinRoomResponse.text();
        })();
    }, [id]);

    useEffect(() => {
        (async () => {
            const roomMessagesResponse = await fetch(`/_matrix/client/v3/rooms/${roomId}/messages?access_token=${accessToken}&dir=b`, {
                method: "GET",
            })
            const { chunk, start, end } = await roomMessagesResponse.json();
            setMessages((chunk ?? []).slice(1));
            setOldestMessageCursor(end);
            setLatestMessageCursor(start);
        })()
    }, [roomId]);

    const sendMessage = useCallback(async () => {
        const sendMessageResponse = await fetch(`/_matrix/client/v3/rooms/${roomId}/send/m.room.message/${nanoid()}?access_token=${accessToken}`, {
            method: "PUT",
            body: JSON.stringify({
                body: String(draftMessage),
                msgtype: "m.text"
            })
        })
    }, [roomId, accessToken, draftMessage]);

    useEffect(() => {
        if (!latestMessageCursor) {
            return;
        }
        const pollMessages = async () => {
            const roomMessagesResponse = await fetch(`/_matrix/client/v3/rooms/${roomId}/messages?access_token=${accessToken}&dir=f&from=${latestMessageCursor}`, {
                method: "GET"
            });
            const { chunk, start, end } = await roomMessagesResponse.json();
            setLatestMessageCursor(end);
            setMessages([ ...(chunk ?? []), ...messages ]);
        }
        pollMessages();
        const messagePoller = setInterval(pollMessages, 2000);
        return () => clearInterval(messagePoller);
    }, [roomId, accessToken, latestMessageCursor]);

    console.log({ data, id });
    const capability = data?.capability ?? {};

    const oldestMessageLoaded = (messages[messages.length - 1]?.type === "m.room.create");
    console.log({
        mlen: messages.length,
        lastmess: messages[messages.length - 1],
        oldestMessageLoaded
    })
    return (<>
        <section className="heading">
            <Breadcrumb>
                <Link to="/">Home</Link>
                <Link to="/capabilities">Capabilities</Link>
                <span>
                    <Ellipsis style={{ width: 90 }}>{capability.id}</Ellipsis>
                </span>
            </Breadcrumb>
            <h1>{capability.title ?? (loading ? <Skeleton /> : <></>)}</h1>
        </section>
        <section className="toolbar">
            <div className="actions">
                <div className="withdraw">
                    <Button isDanger>Discontinue</Button>
                </div>
                <div className="proceed">
                    <Button>Edit</Button>
                    <Button isPrimary>Fulfill</Button>
                </div>
            </div>
        </section>
        <section
            style={{
                display: "flex",
                flexDirection: "row",
                alignItems: "center"
            }}
        >
            <h2
                style={{
                    flex: "1 0 auto"
                }}
            >
                Demand
            </h2>
            <div
                style={{
                    display: "flex",
                    gap: "0.5rem",
                    flexDirection: "row"
                }}
            >
                <span>{"50%"}</span>
                <Button><DashIcon /></Button>
                <Button><PlusIcon /></Button>
            </div>
        </section>
        <section>
            <h2>Details</h2>
            {capability.description ?? (loading && <>
                <Skeleton />
                <Skeleton />
                <Skeleton />
            </>)}
        </section>
        <section>
            <h2>Contact Info</h2>
            {capability.contactInfo ?? (loading && <Skeleton />)}
        </section>
        <section className="conversation">
            {messages.map(({ type, sender, content }) => {
                return (<Message
                    type={type}
                    sender={sender}
                    content={content}
                />);
            })} 
            {oldestMessageLoaded
                ? (<></>)
                : (<Button
                    isDisabled={loadingOlderMessages}
                    style={{
                        alignSelf: "center"
                    }}
                    onClick={() => {
                        setLoadingOlderMessages(true);
                        setTimeout(() => setLoadingOlderMessages(false), 3000)
                    }}
                >
                    {loadingOlderMessages ? "..." : "Load older messages"}
                </Button>)
            }
        </section>
        <section className="composeMessage">
            <Textarea onChange={(e) => {
                setDraftMessage(e.target.value);
            }} />
            <IconButton
                isPrimary
                size="large"
                onClick={sendMessage}
            >
                <PaperAirplaneIcon size={24} />
            </IconButton>
        </section>
    </>);
};
