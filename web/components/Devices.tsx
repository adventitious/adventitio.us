import React from "react";
import { useNavigate, Link } from "react-router-dom";
import gql from "graphql-tag";
import { Button } from "@zendeskgarden/react-buttons";
import { Breadcrumb } from "@zendeskgarden/react-breadcrumbs";
import {
    Body,
    Cell,
    Head,
    HeaderCell,
    HeaderRow,
    Row,
    Table
} from '@zendeskgarden/react-tables';

import { useQuery } from "../../api/graphql/hooks.ts";

import { ListView } from "./ListView.tsx";

export const Devices = () => {

    const navigate = useNavigate();
        
    const { data } = useQuery(gql`
        query Device {
            devices: allDevices {
                id
                ip
                publicKey
            }
        }
    `);

    console.log("in devices component");
    console.log({ data });

    return (<ListView
        loading={!Array.isArray(data?.devices)}
        heading={<>
            <Breadcrumb>
                <Link to="/">Home</Link>
                <span>Devices</span>
            </Breadcrumb>
            <h1>Devices</h1>
        </>}
        data={data?.devices}
        columns={[
            {
                displayName: "ID",
                render: ({ id }: any) => (<Link to={`/devices/${id}`}> {id} </Link>),
                style: {
                    fontWeight: "500",
                    fontFamily: "Fira Mono"
                }
            },
            {
                displayName: "IP",
                render: ({ ip }: any) => ip,
                style: {
                    fontWeight: "500",
                    fontFamily: "Fira Mono"
                }
            },
            {
                displayName: "PublicKey",
                render: ({ publicKey }: any) => publicKey,
                style: {
                    fontWeight: "500",
                    fontFamily: "Fira Mono"
                }
            }
        ]}
        proceedActions={<>
            <Button
                isPrimary
                onClick={() => {
                    navigate("register")
                }}
            >
                Register a Device
            </Button>
        </>}
    />);
};
