import React, {
    useState,
    useEffect
} from "react";
import {
    useParams,
    useSearchParams,
    useNavigate,
    Link
} from "react-router-dom";
import { gql } from "graphql-tag";
import { Button } from "@zendeskgarden/react-buttons";
import { Breadcrumb } from "@zendeskgarden/react-breadcrumbs";
import {
    Field,
    Label,
    Input,
    Textarea,
    Hint
} from "@zendeskgarden/react-forms";
import {
    Field as DropdownField,
    Label as DropdownLabel,
    Hint as DropdownHint,
    Item,
    Menu,
    Dropdown,
    Autocomplete
} from "@zendeskgarden/react-dropdowns";

import { useQuery, useMutation } from "../../api/graphql/hooks.ts";


export const NewRequest = () => {

    const navigate = useNavigate();
    const { id } = useParams(); 
    const [ searchParams, setSearchParams ] = useSearchParams();
    const [ title, setTitle ] = useState("");
    const [ description, setDescription ] = useState("");
    const [ contactInfo, setContactInfo ] = useState("");
    const [ activity, setActivity ] = useState({});
    const [ activities, setActivities ] = useState([]);
    const [ searchActivityTitle, setSearchActivityTitle ] = useState("");

    const {
        mutate: createRequest,
        pending: pendingRequest 
    } = useMutation(gql`
        mutation NewRequest(
            $title: String
            $description: String
            $contactInfo: String
            $activity: ID
        ) {
            createRequest(
                title: $title
                description: $description
                contactInfo: $contactInfo
                activity: $activity
            ) {
                id
            }
        }
    `, {});

    const {
        data: activityData,
        loading: activityLoading,
        refetch: refetchActivity
    } = useQuery(gql`
        query Activity($id: ID!) {
            activity: findActivity(id: $id) {
                id
                title
                description
            }
        }
    `, {
        variables: { id: searchParams.get("activity") }
    });

    const {
        data: activitiesData,
        loading: activitiesLoading,
        refetch: refetchActivities
    } = useQuery(gql`
        query Activities($title: String) {
            activities: allActivities(title: $title) {
                id
                title
                description
            }
        }
    `, {
        variables: { title: searchActivityTitle }
    });

    useEffect(() => {
        if (activityLoading) {
            return false;
        }
        if (activityData.activity === null) {
            return false;
        }
        setActivity(activityData.activity)
    }, [activityData]);

    useEffect(() => {
        if (activitiesLoading) {
            return false;
        }
        if (activitiesData.activity === null) {
            return false;
        }
        setActivities(activitiesData.activities)
    }, [activitiesData, activitiesLoading]);

    return (<>
        <section className="heading">
            <Breadcrumb>
                <Link to="/">Home</Link>
                <Link to="/requests">Requests</Link>
                <span>New Request</span>
            </Breadcrumb>
            <h1>Make a Request</h1>
        </section>
        <section>
            <Field>
                <Label>Title</Label>
                <Hint>Briefly state the request as clearly concisely as possible.</Hint>
                <Input 
                    onChange={(event: any) => {
                        setTitle(event.target.value);
                    }}
                />
            </Field>
            <Dropdown
                inputValue={searchActivityTitle}
                selectedItem={activity}
                onSelect={(activity) => {
                    if (activity.id) {
                        searchParams.set("activity", activity.id);
                        setSearchParams(searchParams);
                        setActivity(activity);
                    }
                }}
                onInputValueChange={async (value: any) => {
                    setSearchActivityTitle(value);
                    await refetchActivities({
                        variables: { title: value }
                    });
                    //setActivities(data.activities);
                }}
                downshiftProps={{ defaultHighlightedIndex: 0 }}
            >
                <DropdownField>
                    <DropdownLabel>Activity</DropdownLabel>
                    <DropdownHint>Select the activity to be performed.</DropdownHint>
                    {activityLoading
                        ? <>loading activity</> 
                        : typeof activityData === "undefined" 
                            ? <>
                                <p>Select the activity which needs to be performed</p>
                            </>
                            : <>
                                <p>Creating a new {activityData.activity.title} Request</p>
                            </>
                    }
                    <Autocomplete>{activity?.title}</Autocomplete>        
                </DropdownField>
                <Menu>
                    {activities.map((_activity) => (
                        <Item
                            key={_activity.id}
                            value={_activity}
                        >
                            <span>{_activity.title}</span>
                        </Item>
                    ))}
                </Menu>
            </Dropdown>
            <Field>
                <Label>Description</Label>
                <Hint>Describe the request in as much detail as possible.</Hint>
                <Textarea
                    isResizable
                    minRows={7}
                    maxRows={17}
                    onChange={(event: any) => {
                        setDescription(event.target.value);
                    }}
                />
            </Field>
            <Field>
                <Label>Contact Info</Label>
                <Hint>Provide information about how best to make contact.</Hint>
                <Textarea
                    isResizable
                    minRows={2}
                    maxRows={5}
                    onChange={(event: any) => {
                        setContactInfo(event.target.value);
                    }}
                />
            </Field>
            <div className="actions">
                <div className="proceed">
                    <Button
                        isPrimary
                        onClick={async () => {
                            const { data } = await createRequest({
                                title,
                                description,
                                contactInfo,
                                activity: activity.id
                            });
                            navigate(`/requests/${data.createRequest.id}`);
                        }}
                    >
                        Submit
                    </Button>
                </div>
            </div>
        </section>
    </>);
};
