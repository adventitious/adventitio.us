import React, {
    useState,
    useEffect
} from "react";
import {
    Routes,
    Route
} from "react-router-dom";

import { AuthContext } from "../context/auth.ts";

import { Layout } from "./Layout.tsx";
import { Index } from "./Index.tsx";
import { Auth } from "./Auth.tsx";
import { 
    Activities,
    Activity,
    NewActivity
} from "./activities.ts";
import { 
    Capabilities,
    Capability,
    NewCapability
} from "./capabilities.ts";
import { Requests } from "./Requests.tsx";
import { Request } from "./Request.tsx";
import { NewRequest } from "./NewRequest.tsx";
import { Devices } from "./Devices.tsx";
import { Device } from "./Device.tsx";
import { DeviceRegistration } from "./DeviceRegistration.tsx";
import { PreferAuthenticated } from "./prefer-authenticated.tsx";
import { NotFound } from "./NotFound.tsx";

export const App = () => {

    const [ guest, setGuest ] = useState();
    const [ username, setUsername ] = useState(localStorage.getItem("username"));
    const [ homeServer, setHomeServer ] = useState(localStorage.getItem("homeServer"));
    const [ deviceId, setDeviceId ] = useState(localStorage.getItem("deviceId"));
    const [ accessToken, setAccessToken ] = useState(localStorage.getItem("accessToken"));

    useEffect(() => {
        console.log({accessToken});
        if (accessToken) {
            return;
        }
        (async () => {
            const authResponse = await fetch("/_matrix/client/v3/register?kind=guest", {
                method: "POST",
                body: JSON.stringify({
                    auth: { type: "m.login.dummy" },
                })
            });

            const responseObject = await authResponse.json();
            console.log({ responseObject });

            const {
                user_id,
                access_token,
                home_server,
                device_id
            } = responseObject;

            setGuest(true);
            setUsername(user_id);
            setAccessToken(access_token);
            setHomeServer(home_server);
            setDeviceId(device_id);

        })();
    }, [accessToken]);

    return (
        <AuthContext.Provider value={{
            guest,
            setGuest,
            username,
            setUsername: (value) => {
                localStorage.setItem("username", value);
                setUsername(value);
            },
            homeServer,
            setHomeServer: (value) => {
                localStorage.setItem("homeServer", value);
                setHomeServer(value);
            },
            deviceId,
            setDeviceId: (value) => {
                localStorage.setItem("deviceId", value);
                setDeviceId(value);
            },
            accessToken,
            setAccessToken: (value) => {
                localStorage.setItem("accessToken", value);
                setAccessToken(value);
            }
        }}>
            <Routes>
                <Route element={<Layout />}>
                    <Route index element={<Index />} />
                    <Route path="auth" element={<Auth />} />
                    <Route path="about" element={
                        <section>
                            <h1>About</h1>
                        </section>
                    } />
                    <Route path="activities" element={<PreferAuthenticated />}>
                        <Route index element={<Activities />} />
                        <Route path="new" element={<NewActivity />} />
                        <Route path=":id" element={<Activity />} />
                    </Route>
                    <Route path="capabilities" element={<PreferAuthenticated />}>
                        <Route index element={<Capabilities />} />
                        <Route path="new" element={<NewCapability />} />
                        <Route path=":id" element={<Capability />} />
                    </Route>
                    <Route path="requests" element={<PreferAuthenticated />}>
                        <Route index element={<Requests />} />
                        <Route path="new" element={<NewRequest />} />
                        <Route path=":id" element={<Request />} />
                    </Route>
                    <Route path="devices">
                        <Route index element={<Devices />} />
                        <Route path="register" element={<DeviceRegistration />} />
                        <Route path=":id" element={<Device />} />
                    </Route>
                    <Route path="*" element={<NotFound />} />
                </Route>
            </Routes>
        </AuthContext.Provider>
    );
};

