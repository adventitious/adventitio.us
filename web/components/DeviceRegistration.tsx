import React from "react";
import { useParams, Link } from "react-router-dom";
import { gql } from "graphql-tag";
import { Button } from "@zendeskgarden/react-buttons";
import { Breadcrumb } from "@zendeskgarden/react-breadcrumbs";
import { Field, Label, Input } from "@zendeskgarden/react-forms";

import { useQuery, useMutation } from "../../api/graphql/hooks.ts";


export const DeviceRegistration = () => {

    const { id } = useParams(); 

    const {
        mutate: registerDevice,
        pending: pendingDeviceRegistration 
    } = useMutation(gql`
        mutation RegisterNode(
            $ip: String
            $name: String
        ) {
            registerDevice(
                ip: $ip
                name: $name
            ) {
                id
            }
        }
    `, {});

    return (<>
        <section className="heading">
            <Breadcrumb>
                <Link to="/">Home</Link>
                <Link to="/devices">Devices</Link>
                <span>Register</span>
            </Breadcrumb>
            <h1>Register a Device</h1>
        </section>
        <section>
            <Field>
                <Label>Nickname</Label>
                <Input />
            </Field>
            <Field>
                <Label>IP Address</Label>
                <Input />
            </Field>
            <div className="actions">
                <div className="proceed">
                    <Button isPrimary
                        onClick={() => {
                            registerDevice({
                                ip: "10.0.0.1",
                                name: "default device"
                            })
                        }}
                    >Register</Button>
                </div>
            </div>
        </section>
    </>);
};
