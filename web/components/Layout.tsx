import React, {
   useContext 
} from "react";
import { Outlet, Link, useNavigate } from "react-router-dom";
import { Button } from "@zendeskgarden/react-buttons";

import { AuthContext } from "../context/auth.ts";

import { Hero } from "./Hero.tsx";


export const Layout = () => {

    const navigate = useNavigate();

    const {
        guest,
        username,
        accessToken,
        setUsername,
        setAccessToken
    } = useContext(AuthContext)

    return (<>
        <header>
            <Link
                to="/"
                style={{
                    color: "#fff",
                    display: "flex",
                    flexDirection: "row",
                    textDecoration: "none",
                    alignItems: "center",
                    gap: "1.5rem"
                }}
            >
                <img
                    style={{
                        height: "3rem",
                        flex: "0 1 3rem"
                    }} 
                    src="/mangrove.png"
                />
                <span
                >
                    adventitious
                </span>
            </Link>
            {guest
                ? (<div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        gap: "0.5rem",
                        fontSize: "1rem"
                    }}
                >
                    <span>Guest</span>
                    <Button
                        isPrimary 
                        size="large"
                        onClick={() => {
                            navigate("/auth");
                        }}
                    >
                        Log In
                    </Button>
                </div>)
                : (<div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        alignItems: "center",
                        gap: "0.5rem",
                        fontSize: "1rem"
                    }}
                >
                    <span>{username}</span>
                    <Button
                        onClick={() => {
                            setAccessToken(undefined); // effectively logs the user out
                            navigate("/")
                        }}
                    >
                        Log Out
                    </Button>
                </div>)
            }
        </header>
        <main>
            <Outlet />
        </main>
    </>);
};

