import React from "react";
import { useParams } from "react-router-dom";
import { gql } from "graphql-tag";

import { useQuery } from "../../api/graphql/hooks.ts";


export const Device = ({
    register
}: {
    register?: boolean;
}) => {

    const { id } = useParams(); 

    const { data } = useQuery(gql`
        query Device {
            device: findDevice(id: $id) {
                id
                name
                ip
                publicKey
            }
        }
    `, {
        id
    });

    console.log({ data });

    return (<>
        <section className="heading">
            <h1>{data?.device?.name ??  ""}</h1>
        </section>
        <section>
            <table>
                {((device: any) => {
                    return (<tr>
                        <td>{device.id}</td>
                        <td>{device.ip}</td>
                        <td>{device.publicKey}</td>
                    </tr>);
                })(data?.device ?? {})}
            </table>
        </section>
    </>);
};
