import React, {
    useContext
} from "react";
import {
    useNavigate,
    Outlet
} from "react-router-dom";
import { Button } from "@zendeskgarden/react-buttons";

import { AuthContext } from "../context/auth.ts";

export const PreferAuthenticated = () => {
    const navigate = useNavigate();
    const { guest } = useContext(AuthContext);

    return <>
        {guest
            ? <section
                style={{
                    backgroundColor: "#ffdd99",
                    flexDirection: "row",
                    paddingBlock: "2rem"
                }}
            >
                <span>
                    To receive notifications when a provider is able to meet requests, or when capabilities are requested, authentication is required. The current session is a guest session, and any information shared will not be attributed with a profile until the session is authenticated.
                </span>
                <Button
                    style={{
                        flex: "1 0 auto"
                    }}
                    onClick={() => {
                        navigate("/auth");
                    }}
                >Authenticate</Button>
            </section>
            : <></>
        }
        <Outlet />
    </>
};

