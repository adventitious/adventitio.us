import React from "react";
import { useNavigate, Link } from "react-router-dom";
import gql from "graphql-tag";
import { Button, ButtonGroup } from "@zendeskgarden/react-buttons";
import { Breadcrumb } from "@zendeskgarden/react-breadcrumbs";
import {
    Body,
    Cell,
    Head,
    HeaderCell,
    HeaderRow,
    Row,
    Table
} from '@zendeskgarden/react-tables';
import { Ellipsis } from '@zendeskgarden/react-typography';
import {
    PlusIcon,
    DashIcon
} from "@primer/octicons-react";

import { useQuery } from "../../api/graphql/hooks.ts";

import { ListView } from "./ListView.tsx";


export const Requests = () => {

    const navigate = useNavigate();
        
    const { data, loading } = useQuery(gql`
        query Requests {
            requests: allRequests {
                id
                title
                description
                contactInfo
                activityId
                activity {
                    id
                    title
                    description
                }
            }
        }
    `);

    console.log("in devices component");
    console.log({ data });

    

    return (<ListView
        loading={loading}
        heading={<>
            <Breadcrumb>
                <Link to="/">Home</Link>
                <span>Requests</span>
            </Breadcrumb>
            <h1>Requests</h1>
        </>}
        data={data?.requests}
        columns={[{
            displayName: "ID",
            render: ({ id, title }: any) => (
                <Link to={`/requests/${id}`}>
                    <Ellipsis style={{ width: 70 }}>{id}</Ellipsis>
                </Link>
            ),
            style: {
                fontWeight: "500",
                fontFamily: "Fira Mono"
            }
        }, {
            displayName: "Title",
            render: ({ id, title }: any) => (<Link to={`/requests/${id}`}>{title}</Link>),
        }, {
            displayName: "Activity",
            render: ({
                activityId,
                activity
            }: any) => (
                <Link to={`/activities/${activityId}`}>
                    {activity?.title ?? "Unknown Activity"}
                </Link>
            ),
        }, {
            displayName: "priority",
            render: ({ id, priority = 0 }: any) => {
                return (<div
                    style={{
                        display: "flex",
                        flexDirection: "row",
                        gap: "0.5rem"
                    }}
                >
                    <Button size="small"><DashIcon /></Button>
                    <Button size="small"><PlusIcon /></Button>
                    <span>{priority}</span>
                </div>);
            }
        }]}
        proceedActions={<>
            <Button
                isPrimary
                onClick={() => {
                    navigate("new")
                }}
            >
                Make a Request
            </Button>
        </>}
    />);
};
