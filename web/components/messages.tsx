import React, {
    useContext
} from "react";
import { AuthContext } from "../context/auth.ts";
import { CanonicalAlias } from "./messages/canonical-alias.tsx";


const Create = () => (
    <div style={{
        color: "#bbb",
        fontWeight: 700,
        fontSize: "1.3rem"
    }}>
        This is the beginning of the conversation
    </div>
)

const JoinRule = ({ content }) => (
    <span className="system-message">
        {{
            public: "This room is free for anyone to join without an invite",
            invite: "This room can only be joined if you were invited",
            knock: "This room can only be joined if you were invited, and allows anyone to request an invite to the room. Note that this join rule is only available in room versions which support knocking"
        }[content.join_rule]}
    </span>
);

const UnknownMessage = ({ sender, content }) => (
    <div className="message">
        <div>{sender}</div>
        <div>{JSON.stringify(content)}</div>
    </div>                                                       
);

const Membership = ({ content }) => (
    <span className="system-message">
        {content.displayname || "Guest"}
        {{
            join: " joined"
        }[content.membership]}
    </span>
);

const PowerLevels = ({ content }) => (
    <span className="system-message">
        Setting power levels
        <pre style={{ display: "none" }}>{JSON.stringify(content)}</pre>
    </span>
);

const HistoryVisibility = ({ content }) => (
    <span className="system-message">
        {{
            shared: "When new users view this conversation, they will be able to read the history"
        }[content.history_visibility]}
    </span>
);

const TextMessage = ({ sender, content }) => {
    const { username } = useContext(AuthContext);
    console.log({ username, sender })
    const messageStyle = username === sender ? {
        alignSelf: "flex-end"
    } : {};

    return (
        <div
            className="message"
            style={{
                ...messageStyle
            }}       
        >
            <div style={{
                fontWeight: 700,
                fontSize: "0.8rem",
                paddingBottom: "0.7rem"
            }}>{sender}</div>
            <div>{content.body}</div>

        </div>
    )
};

const messageTypes = {
    "m.room.create": Create,
    "m.room.message": TextMessage,
    "m.room.join_rules": JoinRule,
    "m.room.canonical_alias": CanonicalAlias,
    "m.room.member": Membership,
    "m.room.power_levels": PowerLevels,
    "m.room.history_visibility": HistoryVisibility,
    default: UnknownMessage
};

export {
    CanonicalAlias
}

export const Message = ({ type, sender, content }) => {
    const MessageComponent = messageTypes[type] ?? messageTypes.default;
    return (<MessageComponent
        type={type}
        sender={sender}
        content={content}
    />)
};

