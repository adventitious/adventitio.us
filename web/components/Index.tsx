import React from "react";
import { Link } from "react-router-dom";

import { Hero } from "./Hero.tsx";


export const Index = () => {
    return (<>
        <Hero/>
        <section>
            <aside
                style={{
                    fontSize: "1.4rem",
                    lineHeight: "2rem"
                }}
            >
                When there are circumstances that require immediate attention, inaction is the only mistake.
            </aside>
            <aside
                style={{
                    fontSize: "1.4rem",
                    lineHeight: "2rem"
                }}
            >
                Sometimes it can be difficult to take initiative. We fear we don't have all the information to make the right decision, and that we might make a mistake. We fear our actions may be judged by others, or that we might even make things worse than they were in the first place.
            </aside>
            <aside
                style={{
                    fontSize: "1.4rem",
                    lineHeight: "2rem"
                }}
            >
                Action is enabled when we foster a culture of investigation to find out what is needed, when we share and distribute resources and instructions on how to provide it, and crucially, when we give ourselves the permission to take the risk.
            </aside>
        </section>
        <section>
            <h1>Discover Possibilities</h1>
            <div
                className="details"
            >
                <img
                    src="/undraw_playing_fetch_cm19.svg"
                    style={{
                        width: "12rem",
                    }}
                />
                <div
                    style={{
                        flex: "0 1 auto"
                    }}
                >
                    <p>People are capable of awesome and suprising things. As people, we engage in myriad activities, perform all sorts of services for each other's benefit. We also may have our own special interests or hobbies. Documenting and cataloging these different kinds of activities, services and interests, more people are able to discover activities and interests they may not have known about, and people are able to discover activities and services that they might not realize they have a need for.</p>
                    <aside>
                        <Link to="/activities">Discover activities and services that the network enables the distribution of.</Link>
                    </aside>
                </div>
            </div>
        </section>
        <section>
            <h1>Providers</h1>
            <div
                className="details"
            >
                <img src="/undraw_deliveries_2r4y.svg"
                    style={{
                        width: "12rem"
                    }}
                />
                <div
                    style={{
                        flex: "0 1 auto"
                    }}
                >
                    <p>When someone is interested on performing an activity for the benefit of another, it can be time consuming or overwhelming to find people that need that activity or service performed. By sharing information about skill level, and elaboriating hon any specialization at a skill or sevice helps a requestor understand what is capable of being provided and also sets expectations in a clear way. When we detail our capabilities it also serves as a useful tool to share what we're good at and what we prefer to do with our energy</p>
                    <aside>
                        <Link to="/capabilities">Create capabilities to provide resources or deliver services.</Link>
                    </aside>
                </div>
            </div>
        </section>
        <section>
            <h1>Request</h1>
            <div
                className="details"
            >
                <img
                    src="/undraw_project_feedback_re_cm3l.svg"
                    style={{
                        width: "12rem"
                    }}
                />
                <div>
                    <p>Community is a critical resource for survival because of situations where a person not be able to provide their own needs. While in many cases it is possible to meet ones own needs in a self sufficient manner, it's important to understand that in many cases this is impossible or detrimental</p>
                    <p>Needs are the single most motivating factor for an individual when we need something, we know what we need and why. Basic needs are often the most critical, and should be provided to everyone equally. Pleasure and luxury, while not as urgent and critical as basic needs, are still valid needs, and may be provided as compensation for meeting other's needs.</p>
                    <aside>
                        <Link to="/requests">Request the provision or delivery of services or resources.</Link>
                    </aside>
                </div>
            </div>
        </section>
        <section>
            <h1>Network</h1>
            <div
                className="details"
            >
                <img
                    src="/undraw_devices_re_dxae.svg"
                    style={{
                        width: "12rem"
                    }}
                />
                <div>
                    <p>Communication between groups, known as federation, enables better flexibility and more options to help respond to needs.</p>
                    <p>Communities already exist that provide mutual aid resources such as food banks or discord servers and telegram groups. Small communities have a benefit of members having the time to deeply understand what one another's needs are, and are enabled to focus on a particular subject or specialty. Larger communities benefit from having a wide diversity of individuals that each have different specialties.</p>
                    <p>By registering discord servers, slack channels, or even a custom applications with each other as devices, federation enables communities to discover each others specialties and resources.</p>
                    <aside>
                        <Link to="/devices">Discover other devices and servers that participate in the network and how updates are transimtted.</Link>
                    </aside>
                </div>
            </div>
        </section>
        <section>
            <h1>Discover Common Goals</h1>
            <div
                className="details"
            >
                <img
                    src="/undraw_shared_goals_re_jvqd.svg"
                    style={{
                        width: "12rem"
                    }}
                />
                <div style={{ flex: "0 1 auto"}}>
                    <p>Adventitious is a small part of a wide community of projects that seek to enable everyone to take initiative. Even though the resources these communities provide might not be networked, the resources are really valuable and sharing these resources here helps with their discoverability until a time federation is enabled for their resources.</p>
                </div>
            </div>
            <ul class="item-grid">
                <li><a href="https://democracy.earth">Democracy Earth</a></li>
                <li><a href="https://bfi.org">Buckminster Fuller Institute</a></li>
                <li><a href="https://regenerosity.world">Regenerosity</a></li>
                <li><a href="https://juvare.com">Juvare</a></li>
                <li><a href="https://mutualaidhub.org">Mutual Aid Hub</a></li>
                <li><a href="https://communiteacaravan.com">Communitea Caravan</a></li>
                <li><a href="https://wwoof.net">WWOOF</a></li>
                <li><a href="https://bio-hab.org">Bio-Hab</a></li>
                <li><a href="https://bigmotherdao.com/">Big Mother Dao</a></li>
                <li><a href="https://www.humanetech.com/">Center for Humane Technology</a></li>
                <li><a href="https://mutualaidnetwork.org">Mutual Aid Network</a></li>
                <li><a href="http://onefreeapp.com">OneFreeApp</a></li>
                <li><a href="https://gnomadic.com">Gnomadic</a></li>
                <li><a href="https://sustainablecostarica.org">Sustainable Costa Rica</a></li>
                <li><a href="https://mangrovealliance.org">Mangrove Alliance</a></li>
            </ul>
        </section>
        <section>
            <h1>Technology Providers</h1>
            <div class="details">
                <img src="/undraw_inspiration_re_ivlv.svg"
                    style={{
                        width: "12rem"
                    }}
                />
                <div>
                    <p>Technology and tools are a force multiplier; they increase productivity by enabling an individual to do more with less effort.</p>
                </div>
            </div>
            <ul class="item-grid">
                <li><a href="https://esri.com">esri</a></li>
                <li><a href="https://juvare.com">Juvare</a></li>
                <li><a href="https://plaas.io">Plaas</a></li>
                <li><a href="https://libp2p.io">libp2p</a></li>
                <li><a href="https://matrix.org">Matrix</a></li>
                <li><a href="https://ipfs.io">IPFS</a></li>
                <li><a href="https://osf.io">OSF</a></li>
                <li><a href="https://ignite.com">Ignite</a></li>
                <li><a href="https://cosmos.network">Cosmos</a></li>
                <li><a href="https://deno.land">Deno</a></li>
                <li><a href="https://reactjs.org">React</a></li>
            </ul>
        </section>
    </>)
};
