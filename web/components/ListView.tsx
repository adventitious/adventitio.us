import React from "react";
import { useNavigate, Link } from "react-router-dom";
import gql from "graphql-tag";
import { Button } from "@zendeskgarden/react-buttons";
import { Breadcrumb } from "@zendeskgarden/react-breadcrumbs";
import {
    Body,
    Cell,
    Head,
    HeaderCell,
    HeaderRow,
    Row,
    Table
} from '@zendeskgarden/react-tables';

import { useQuery } from "../../api/graphql/hooks.ts";

export const ListView = ({
    loading = false,
    heading,
    proceedActions,
    columns,
    data = []
}: any) => {

    const tableHead = (<Head>
        <HeaderRow>{columns.map(({ displayName }: any) => (
            <HeaderCell>{displayName}</HeaderCell>
        ))}</HeaderRow>
    </Head>);

    const tableBody = (loading ? <></> : <Body>{data.map((item: any) => (
        <Row>{columns.map(({ render, truncate=false }: any) => (
            <Cell 
                isTruncated={truncate}
                style={item.style}
            >
                {render(item)}
            </Cell>
        ))}</Row>
    ))}</Body>);

    const table = (<Table>
        {tableHead}
        {tableBody}
    </Table>);

    return (<>
        <section className="heading"> {heading} </section>
        <section className="toolbar">
            <div className="actions">
                <div className="proceed"> {proceedActions} </div>
            </div>
        </section>
        <section>
            {loading ? "...loading" : table}
        </section>
    </>);
};
