import postcss from "https://deno.land/x/postcss/mod.js";
import autoprefixer from "https://esm.sh/autoprefixer"

const style = await postcss([
    autoprefixer
]).process();
