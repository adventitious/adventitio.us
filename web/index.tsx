//import { init } from "https://deno.land/x/denofill/mod.ts";

//await init();

import React from "react";
import { hydrate } from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { DEFAULT_THEME, ThemeProvider } from '@zendeskgarden/react-theming';

import { schema } from "../api/index.ts";

import { GraphQLClient } from "../api/graphql/client.tsx";
import { GraphQLProvider } from "../api/graphql/provider.tsx";
import { HttpLink } from "../api/graphql/link.ts";

import { App } from "./components/App.tsx";

//import "https://esm.sh/@zendeskgarden/css-bedrock/dist/index.css";


hydrate(
    <ThemeProvider theme={{
        ...DEFAULT_THEME
    }}>
        <GraphQLProvider client={new GraphQLClient({
            link: new HttpLink({ uri: "/graphql" })
        })}>
            <BrowserRouter>
                <App />
            </BrowserRouter>
        </GraphQLProvider>
    </ThemeProvider>,
    document.getElementById("root")
);

