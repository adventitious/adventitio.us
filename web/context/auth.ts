import React, {
    createContext
} from "react";

export const AuthContext = createContext({
    username: null,
    homeServer: null,
    deviceId: null,
    accessToken: null,
    setUsername: () => {},
    setHomeServer: () => {},
    setDeviceId: () => {},
    setAccessToken: () => {}
});

