
import { GraphQLLink, HttpLink } from "./link.ts";

const defaultLink = new HttpLink();

export interface GraphQLClientQueryOptions {
    variables?: any;
    operationName?: string;
}

export interface GraphQLClient {
    query(query: any, options?: GraphQLClientQueryOptions): Promise<any>;
    querySync(query: any, options?: GraphQLClientQueryOptions): any;
}

export interface GraphQLClientOptions {
    uri?: string;
    link?: GraphQLLink;
    cache?: any;
    typeDefs?: any;
    renderPromises?: Map<string, Promise<any>>
}

export class GraphQLClient {

    link: GraphQLLink;
    cache: any;
    typeDefs: any;
    renderPromises: any;

    constructor({
        uri,
        link = defaultLink,
        cache,
        typeDefs,
        renderPromises
    }: GraphQLClientOptions) {
        this.link = link;
        this.cache = cache;
        this.typeDefs = typeDefs;
        this.renderPromises = renderPromises ?? new Map();
    }

    async query(
        query: any,
        { variables, operationName }: GraphQLClientQueryOptions = {}
    ) {
        console.log({variables})
        return await this.link.execute({ query, variables, operationName });
    }

}

