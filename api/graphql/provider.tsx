import type { ReactNode, Context, FC } from "react";
import React, { createContext } from "react";
import { GraphQLClient } from "./client.tsx";


export interface GraphQLContextValue { 
    client?: GraphQLClient | {};
    renderPromises?: Map<string, Promise<any>>;
}

export const GraphQLContext = createContext<GraphQLContextValue>({});

export interface GraphQLProviderProps {
    client: GraphQLClient;
    children: ReactNode | ReactNode[] | null;
    renderPromises?: Map<string, Promise<any>>;
}

export const GraphQLProvider: FC<GraphQLProviderProps> = ({
    children,
    client,
    renderPromises
}) => {
    //const { } = useContext(GraphQLContext);
    return <GraphQLContext.Provider value={{
        client,
        ...(renderPromises ? { renderPromises }: {})
    }}>
        {children}
    </GraphQLContext.Provider>
};

