import {
    execute,
    visit,
    DefinitionNode,
    VariableDefinitionNode,
    GraphQLSchema,
    print
} from "graphql";

interface ExcludingUnusedParameters {
    query: any;
    variables: Record<string, any>;
}

const excludingUnused = (
    { query, variables }: ExcludingUnusedParameters
): Record<string, any> => {
    if (!variables) {
        return {};
    }
    const usedNames: Set<string> = new Set();
    visit(query, {
        Variable(node, _, parent) {
            if ((parent as VariableDefinitionNode)?.kind === "VariableDefinition") {
                usedNames.add(node.name.value as string);
            }
        }
    });
    return [...usedNames].reduce((usedVariables: Record<string, any>, name: string) => {
        Reflect.set(usedVariables, name, variables[name]);
        return usedVariables;
    }, {});
};

export interface GraphQLLinkExecuteParameters {
    query?: any;
    variables?: any;
    operationName?: string;
}

export interface GraphQLLink {
    execute(parameters: GraphQLLinkExecuteParameters): Promise<any>;
}

export class GraphQLLink {};

export interface HttpLink extends GraphQLLink {}

export interface HttpLinkOptions {
    uri?: string;
    includeExtensions?: boolean;
    headers?: any;
    credentials?: string;
    fetchOptions?: any;
}


export class HttpLink extends GraphQLLink {

    uri: string;
    headers: any;
    credentials: string;
    fetchOptions: any;

    constructor({
        uri = "/graphql",
        includeExtensions = false,
        headers = {},
        credentials = "",
        fetchOptions = {}
    }: HttpLinkOptions | undefined = {}) {
        super();
        this.uri = uri;
        this.headers = headers;
        this.credentials = credentials;
        this.fetchOptions = fetchOptions;
    }

    async execute({ query, variables, operationName }: GraphQLLinkExecuteParameters) {
        console.log("executing http link")
        const response = await fetch(this.uri, {
            method: "POST",
            body: JSON.stringify({
                operationName,
                query: print(query),
                variables: excludingUnused({ query, variables })
            }),
            ...(this.credentials ? { credentials: this.credentials } : {}),
            ...this.fetchOptions
        });
        return await response.json();
    }

};

export interface SchemaLink extends GraphQLLink {};
export interface SchemaLinkOptions {
    schema: GraphQLSchema;
    rootValue?: any;
    context?: Record<string, any>;
    validate?: boolean;
}

export class SchemaLink extends GraphQLLink {

    schema: GraphQLSchema;
    rootValue: any;
    context: Record<string, any> | undefined;
    validate: boolean;

    constructor({ schema, rootValue, context, validate }: SchemaLinkOptions) {
        super();
        this.schema = schema;
        this.rootValue = rootValue;
        this.context = context;
        this.validate = Boolean(validate);
    }

    async execute({ query, variables, operationName }: GraphQLLinkExecuteParameters) {
        console.log("executing schema link")
        return await execute({
            schema: this.schema,
            document: query,
            rootValue: this.rootValue,
            contextValue: this.context,
            variableValues: variables,
            operationName
        });
    }

}

