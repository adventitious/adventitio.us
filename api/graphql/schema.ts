import {
    GraphQLSchema,
    GraphQLObjectType
} from "graphql";

import { types } from "../../lib/types/index.ts";


const buildField = ({ receiver, propertyName, descriptor }: any) => {
    const args = descriptor.parameters({ receiver });
    const returns = descriptor.returns({ receiver });
    //const resolve = type.liftResolver(descriptor)
    const resolve = descriptor.liftResolver({ receiver, method: descriptor.value });
    return { type: returns, args, resolve };
};

const makeFieldAggregator = (fieldsGetter: any) => (types: any) => {
    return types.reduce((queryFields: any, type: any) => {
        return [...fieldsGetter(type)].reduce((
            queryFields: any,
            [ propertyName, descriptor ]: any[]
        ) => {
            const name = descriptor.name({ receiver: type });
            queryFields[name] = buildField({
                receiver: type,
                propertyName, descriptor
            }); 
            return queryFields;
        }, queryFields);
    }, {});
};

const aggregateQueryFields = makeFieldAggregator((type: any) => type.queries);
const aggregateMutationFields = makeFieldAggregator((type: any) => type.mutations);
const aggregateSubscriptionFields = makeFieldAggregator((type: any) => type.subscriptions);

const queryFields = aggregateQueryFields(types);
const mutationFields = aggregateMutationFields(types);
const subscriptionFields = aggregateSubscriptionFields(types);

console.log(queryFields);

export const schema = new GraphQLSchema({ 
    ...((Object.keys(queryFields).length > 0 ? { 
        query: new GraphQLObjectType({
            name: "Query",
            fields: queryFields
        })
    } : {}) as any),
    ...((Object.keys(mutationFields).length > 0 ? {
        mutation: new GraphQLObjectType({
            name: "Mutation",
            fields: mutationFields
        })
    } : {}) as any),
    ...((Object.keys(subscriptionFields).length > 0 ? {
        subscription: new GraphQLObjectType({
            name: "Subscription",
            fields: subscriptionFields
        })
    } : {}) as any)
});

