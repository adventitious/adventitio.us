import { 
    useContext, 
    useState,
    useEffect,
    useRef,
    useCallback
} from "react";
import { DocumentNode } from "graphql";

import { GraphQLClient } from "./client.tsx";
import { GraphQLContext, GraphQLContextValue } from "./provider.tsx";


export const useQuery = (query: DocumentNode, options?: any) => {
   
    const { client, renderPromises } = useContext(GraphQLContext) as GraphQLContextValue ;
    const queryId = JSON.stringify({ query, options })
    const pendingQuery = renderPromises?.get(queryId) ?? (client as GraphQLClient).query(query, options);
    const [ data, setData ] = useState((pendingQuery as unknown as { result: any; }).result?.data);
    const [ loading, setLoading ] = useState(!Boolean((pendingQuery as unknown as { result: any; }).result?.data));
   
    const refetch = useCallback(async (overrideOptions = {}) => {
        setLoading(true);
        const { data } = await client.query(query, {
            ...options,
            ...overrideOptions
        })
        setData(data);
        setLoading(false);
        return {
            data
        }
    }, [query, options])

    useEffect(() => {
        if (!loading) {
            return;
        }
        (async () => {
            const { data } = await pendingQuery;
            setData(data);
            setLoading(false);
        })()
        return () => {};
    }, [query]);
   
    if (!(pendingQuery as unknown as { done: boolean }).done) {
        renderPromises?.set(queryId, pendingQuery);
    }

    return {
        data,
        loading,
        refetch
    };

};

export const useMutation = (mutation: DocumentNode, options?: any) => {

    const { client } = useContext(GraphQLContext) as GraphQLContextValue;
    const [ pending, setPending ] = useState(false);
    const pendingMutations = useRef<any[]>([]);

    const mutate = useCallback((variables: any = options?.variables) => {
        const pendingMutation = (client as GraphQLClient).query(mutation, {
            ...options,
            variables
        });
        pendingMutations.current.push(pendingMutation);
        setPending(true);
        return pendingMutation;
    }, [mutation, client]);

    useEffect(() => {
        (async () => {
            while (pendingMutations.current.length > 0) {
                await pendingMutations.current.unshift();
            }
            setPending(false);
        });
    }, [pending]);

    return { mutate, pending };

}
