import { tableize } from "inflected";
import { gql } from "graphql-tag";
import { nanoid } from "nanoid";

import { env } from "../env.ts";

import { schema } from "./graphql/schema.ts";
export { GraphQLClient } from "./graphql/client.tsx";
export { GraphQLProvider } from "./graphql/provider.tsx";
export { SchemaLink } from "./graphql/link.ts";


export { schema };

export const typeDefs = gql`
    type Node {
        id: String
        ip: String
        publicKey: String
    }
    type Query {
        listNodes(limit: Int, offset: Int): [Node]
        getNode(id: ID!): Node
    }
    type Mutation {
        registerNode(
            ip: String,
            publicKey: String
        ): Boolean
    }
`;


const tableizeKeys = (data: any) => {
    Object.entries(data).map(([key, value]) => [tableize(key), value])
};

export const serveGraphQLApi = await (async () => {
    if (env.RUNTIME === "deno") {
        const { GraphQLHTTP } = await import("gql");
        return GraphQLHTTP({ schema, graphiql: true });
    }
    return undefined as any;
})();

