import { ConnInfo }from "http/server";

export const serveRequestIp = (requst: Request, connInfo: ConnInfo) => {
    const { hostname, port } = connInfo.remoteAddr as Deno.NetAddr;
    return new Response(`${hostname}:${port}`);
};

