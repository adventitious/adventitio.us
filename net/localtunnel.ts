
const getUrl = retry(async ({ uri }) => {
    const response = await fetch(uri);
    if (response.status !== 200) {
        throw new Error((await response.json()).message ?? "localtunnel returned an error")
    }
    return await response.json()
});


Deno.connect({
    hostname: "localtunnel.me
    port: 9000
})


class HeaderHostTransformer extends Transform {
    constructor(opts = {}) {
        super(opts);
        this.host = opts.host ?? "localhost";
        this.replaced = false;
    } 

    _transform(data, encoding, callback) {
        if (this.replaced) {
            return callback(null, data);
        }
        const hostHeader = data.toString().replace(/(\r\n[Hh]ost: )\S+/, (match, $1) => {
            this.replaced = true;
            return $1 + this.host;
        })
        const arg = this.replaced
        callbacj(
    }
}

class TunnelCluster extends EventEmitter {
    constructor(opts = {}) {
        super(opts);
        this.opts = opts;
    }

    open() {
        const remoteHostOrIp = this.opts.remoteIp ?? this.opts.remoteHost;
        const remotPort = this.opts.remmotePort;
        const localHost = this.opts.localHost ?? "localhost";
        const localPort = this.opts.localPort;
        const localProtocol = this.opts.localHttps ? "https" : "http";
        const allowInvalidCert = this.opts.allowInvalidCert;
        const remote = net.connect({
            host: remoteHostOrIp,
            port: remotePort
        });
        remote.setKeepAlive(true);
        remote.on("error", (error) => {
            if (error === "ECONNREFUSED") {
                this.emit("error", new Error("Connection Refused"));
            }
            remote.end();
        });

        const connlocal = () => {
            if (remote.destroyed) {
                this.emit("dead");
                return;
            }

            remote.pause();
            const getLocalCertOpts = () => {
                if (allowInvalidCert) {
                    return { rejectUnauthorized: false };
                }
                return {
                    cert: Deno.readFileSync(this.opts.localCert),
                    key: Deno.readFileSync(this.opts.localKey),
                    ca: this.opts.localCa ? [Deno.readFileSync(this.opts.localCa)] : undefined
                };
            };
            const local = this.opts.localHttps
                ? tls.connect({ host: localHost, port: localPort, ...getLocalCertOpts() })
                : net.connect({ host: localHost, port: localPort });

            const remoteClose = () => {
                this.emit("dead");
                local.end();
            }

            remote.once("close", remoteClose);
            local.once("error", (error) => {
                local.end();
                remote.removeListener("close", remoteClose);
                if (error.code !== "ECONNREFUSED") {
                    return remote.end();
                }
                setTimeout(connLocal, 1000);
            });

            local.once("connect", () => {
                remote.resume();
                let stream = remote;

                if (this.opts.localHost) {
                    stream = remote.pipe(new HeaderHostTransformer({
                        host: this.opts.localHost
                    })
                }
                stream.pipe(local).pipe(remote);
            });

        };

        remote.on("data", (data) => {
            const match = data.toString().match(/^(\w+) (\S+)/);
            if (match) {
                this.emit("request", {
                    method: match[1],
                    path: match[2]
                });
            }
        });


        remote.once("connect", () => {
            this.emit("open", remote);
            connLocal();
        })

    }

}

class Tunnel extends EventEmitter {
    static create() {
        return new this();
    }

    constructor(opts = {}) {
        super(opts);
        this.opts = opts;
        this.closed = false;
        if (!this.opts.host) {
            this.opts.host = "https://localtunnel.me";
        }
    }

    _getInfo({
        id, 
        ip, 
        port, 
        url,
        cached_url: cachedurl,
        max_conn_count: maxConnCount = 1
    }) {
        return {
            name: id,
            url,
            cachedUrl,
            maxConnCount,
            remoteHost: new URL(this.opts.host).hostname,
            remoteIp: ip,
            remotePort: port,
            localPort: this.opts.port,
            localhost: this.opts.local_host,
            localHttps: this.opts.local_https,
            localCert: this.opts.local_cert,
            localKey: this.opts.local_key,
            localCa: this.opts.local_ca,
            allowInvalidCert: this.opts.allowInvalidCert
        }
    }

    _init() {
        return this._getinfo(getUrl({
            uri: `${this.opts.host}/${this.opts.subdomain ?? "?new"}`
        }));
    }

    _establish({ maxConn }) {
        this.setMaxListeners(maxConn + (EventEmitter.defaultMaxListeners || 10));
        this.tunnelCluster = new TunnelCluster(info);
        this.tunnelcluster.once("open", () => {
            this.emit("url", info.url);
        })
        this.tunnelCluster.on("error", (error) => {
            this.emit("error", error);
        });
        this.tunnelCount = 0;
        this.tunnelCluster.on("open", (tunnel) => {
            tunnelCount++;
            if (this.closed) {
                return tunnel.destroy();
            }
            this.once("close", ::tunnel.destroy);
            tunnel.once("close", () => this.removeListener("close", ::tunnel.destroy));

        });

        this.tunnelCluster.on("dead", () => {
            tunnelCount--;
            if (this.closed) {
                return;
            }
            this.tunnelCluster.open();
        });

        this.tunnelCluster.on("request", (request) => {
            this.emit("request", request);
        }); 

        Array.from({ length: info.maxConn }).forEach(() => {
            this.tunnelCluster.open();    
        })

    }

    async open() {
        try {
            const info = await this._init();
        } catch (error) {
            return error
        }
        this.clientId = info.name;
        this.url = info.url;
        this.cachedUrl = info.cachedUrl ?? this.cachedUrl;
        this._establish(info)
    }

    close() {
        this.closed = true;
        this.emit("close");
    }

}

export const localtunnel = () => {


}
