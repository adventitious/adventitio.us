
const username = "username";
const password = "password";
const hostname = "hostname";

let ip;

const performUpdate = (ip) => {
    const uri = `https://${username}:${password}@domains.google.com/nic/update?hostname=${hostname}`
    const response = await fetch(uri, {
        method: "POST"
        headers: {
            "User-Agent": "atri-al"
        }
    });
}



const watchMyIp = () => setInterval(async () => {
    try {
        const latestIp = await (await fetch("https://www.cloudflare.com/cdn-cgi/trace")).text();
        if (!latestIp.match(/)[0]) {
            return;
        }
        if (latestIp !== ip) {
            await performUpdate()
            ip = latestIp
            return;
        }
    } catch (error) {
        consle.log(`Didnt update the ip because ${error}`);
    }
}, 1000);

