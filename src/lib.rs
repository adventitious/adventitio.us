use tokio::runtime;
use tokio::runtime::Runtime;
use state::Storage;
use url::Url;
use deno_bindgen::deno_bindgen;
use ruma::{
    events::room::message::{
        MessageType,
        RoomMessageEventContent,
        SyncRoomMessageEvent,
        TextMessageEventContent
    }
};
use matrix_sdk::{
    room::Room,
    config::SyncSettings
};

static RUNTIME: Storage<Runtime> = Storage::new();

#[deno_bindgen]
fn initialize() {
    RUNTIME.set(runtime::Builder::new_multi_thread()
        .worker_threads(3)
        .enable_time()
        .enable_io()
        .build()
        .unwrap()
    );
}

async fn on_room_message(event: SyncRoomMessageEvent, room: Room) {
    if let Room::Joined(room) = room {
        if let SyncRoomMessageEvent {
            content: RoomMessageEventContent {
                msgtype: MessageType::Text(TextMessageEventContent { body: msg_body, .. }),
                ..
            },
            sender,
            ..
        } = event {
            let member = room.get_member(&*sender).await.unwrap().unwrap();
            let name = member.display_name().unwrap_or_else(|| member.user_id().as_str());
            println!("{}:{}", name, msg_body)
        }
    }
}

#[deno_bindgen]
fn login(
    homeserver_url: &str,
    username: &str,
    password: &str
) ->  u8 {
    RUNTIME.get().block_on(async {
        let homeserver_url = Url::parse(&homeserver_url).expect("Couldnt parse url");
        let client = matrix_sdk::Client::builder()
            .homeserver_url(homeserver_url)
            .disable_ssl_verification()
            .build()
            .await.unwrap();

        println!("got client");
        client.register_event_handler(on_room_message).await;
        println!("registered event handler");
        client.login(username, password, None, Some("rust-sdk")).await.ok();
        println!("logged in");
        client.sync(SyncSettings::default()).await;
        println!("did sync");
    });
    return 1;
}

