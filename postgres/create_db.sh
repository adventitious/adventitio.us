#!/bin/sh

echo "INITIALIZING THE DATABASE\n"

psql -U postgres <<EOF
    CREATE DATABASE dendrite;
    CREATE ROLE dendrite WITH CREATEROLE CREATEDB LOGIN PASSWORD 'itsasecret';
EOF

for db in userapi_accounts mediaapi syncapi roomserver keyserver federationapi appservice mscs; do
    createdb -U dendrite -O dendrite dendrite_$db
done
