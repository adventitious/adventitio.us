FROM denoland/deno


RUN mkdir /deno-dir/gen && chmod 777 /deno-dir/gen
RUN mkdir /deno-dir/deps && chmod 777 /deno-dir/deps
RUN mkdir /deno-dir/deps/https && chmod 777 /deno-dir/deps/https
RUN chown -R deno /deno-dir

EXPOSE 9000
WORKDIR /app

COPY deno.config.json deno.config.json
COPY import_map.json import_map.json
COPY nessie.config.ts nessie.config.ts
COPY types types
COPY scripts scripts
COPY public public
COPY src src
COPY api api
COPY web web
COPY db db
COPY net net
COPY lib lib
COPY env.ts env.ts
COPY server server

RUN ls -la /
RUN ls -la /deno-dir

RUN mkdir dist && chmod 777 dist
USER deno

CMD [ "./scripts/run" ]

