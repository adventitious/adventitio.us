import {
    AbstractSeed,
    ClientPostgreSQL,
    Info,
} from "https://deno.land/x/nessie/mod.ts";

export default class extends AbstractSeed<ClientPostgreSQL> {
    async run({ dialect }: Info): Promise<void> {

        const transaction = await this.client.createTransaction("seed-activities-adv-1");
        await transaction.begin();
        await Promise.all([
            {
                id: "adv-1-1",
                title: "Provide Water",
                description: "Provide water to an individual or group that needs it."
            },
            {
                id: "adv-1-2",
                title: "Provide Food",
                description: "Provide food to an individual or group that needs it."
            },
            {
                id: "adv-1-3",
                title: "Provide Fuel",
                description: "Provide fuel to an individual or group that needs it."
            },
            {
                id: "adv-1-4",
                title: "Provide Transport",
                description: "Provide transportation of a resource, individual, or group to an individual or group that needs it."
            },
            {
                id: "adv-1-5",
                title: "Provide Shelter",
                description: "Provide shelter for an individual or group that needs it."
            },
            {
                id: "adv-1-6",
                title: "Provide Shelter",
                description: "Provide shelter for an individual or group that needs it."
            },
            {
                id: "adv-1-7",
                title: "Provide Communication",
                description: "Provide an individual with tools for communication such as a cell phone."
            },
            {
                id: "adv-1-8",
                title: "Provide Compute",
                description: "Provide an individual with tools for computation such as a computer."
            },
            {
                id: "adv-1-9",
                title: "Healthcare Coordination",
                description: "Provide an individual navigate the complexities of finding doctors, making appointments, or talking to insurance providers."
            },
            {
                id: "adv-1-9",
                title: "Walk a pet",
                description: "Take a pet that needs exercise or companionship for a walk."
            },
            {
                id: "adv-1-10",
                title: "Body Doubling",
                description: "Support an individual that can focus or be more productive when other people are around by being present, either physically or virtually, while the individual is completing a task."
            },
            {
                id: "adv-1-11",
                title: "Research",
                description: "Collect analyze and/or interpret information."
            }
        ].map(async ({
            id,
            title,
            description
        }) => {
            console.log({
                id,
                title,
                description
            });
            return await transaction.queryObject`
                INSERT INTO activities (
                    id,
                    title,
                    description
                ) VALUES (
                    ${id},
                    ${title},
                    ${description}
                ) ON CONFLICT (id) DO NOTHING
            `;
        }));
        await transaction.commit();
    }
};
