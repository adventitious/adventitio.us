import {
    AbstractSeed,
    ClientPostgreSQL,
    Info,
} from "https://deno.land/x/nessie/mod.ts";

export default class extends AbstractSeed<ClientPostgreSQL> {
    async run({ dialect }: Info): Promise<void> {

        const transaction = await this.client.createTransaction("seed-gnomadic-1-activities");
        await transaction.begin();
        await Promise.all([{
            id: "gnome-1-1",
            title: "Build A Dome",
            description: "Build a Gnome dome."
        }, {
            id: "gnome-1-2",
            title: "Install Dome Septic",
            description: "Set up and install septic for a Gnome Dome."
        }, {
            id: "gnome-1-3",
            title: "Provide Land",
            description: "Dedicate a portion of land to be a space to build a Gnome dome on."
        }].map(async ({
            id,
            title,
            description
        }) => {
            console.log({
                id,
                title,
                description
            });
            return await transaction.queryObject`
                INSERT INTO activities (
                    id,
                    title,
                    description
                ) VALUES (
                    ${id},
                    ${title},
                    ${description}
                ) ON CONFLICT (id) DO NOTHING
            `;
        }));
        await transaction.commit();
    }
};
