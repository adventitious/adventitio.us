import {
    AbstractMigration,
    ClientPostgreSQL
} from "nessie";


export default class extends AbstractMigration<ClientPostgreSQL> {

    async up(): Promise<void> {
        await this.client.queryObject(`
            CREATE TABLE devices (
                id varchar(64),
                ip varchar(64),
                publicKey varchar(2048)
            )
        `);
    }

    async down(): Promise<void> {
        await this.client.queryObject(`
            DROP TABLE devices
        `)
    }

}

