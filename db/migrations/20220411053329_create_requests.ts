import { AbstractMigration, Info, ClientPostgreSQL } from "https://deno.land/x/nessie@2.0.5/mod.ts";

export default class extends AbstractMigration<ClientPostgreSQL> {
    /** Runs on migrate */
    async up(info: Info): Promise<void> {
        await this.client.queryObject(`
            CREATE TABLE requests (
                id varchar(64),
                title varchar(512),
                description text,
                contact_info text
            )
        `);
    }

    /** Runs on rollback */
    async down(info: Info): Promise<void> {
        await this.client.queryObject(`
            DROP TABLE requests
        `)
    }
}
