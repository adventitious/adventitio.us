import { AbstractMigration, Info, ClientPostgreSQL } from "https://deno.land/x/nessie@2.0.5/mod.ts";

export default class extends AbstractMigration<ClientPostgreSQL> {
    /** Runs on migrate */
    async up(info: Info): Promise<void> {
        await this.client.queryObject(`
            ALTER TABLE requests
            ADD withdrawn boolean NOT NULL
            DEFAULT false
        `);
    }

    /** Runs on rollback */
    async down(info: Info): Promise<void> {
        await this.client.queryObject(`
            ALTER TABLE requests
            DROP withdrawn
        `);
    }
}
