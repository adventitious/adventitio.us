import { AbstractMigration, Info, ClientPostgreSQL } from "https://deno.land/x/nessie@2.0.5/mod.ts";

export default class extends AbstractMigration<ClientPostgreSQL> {
    /** Runs on migrate */
    async up(info: Info): Promise<void> {
        await this.client.queryObject(`
            CREATE TABLE accounts (
                id varchar(64),
                name varchar(512),
                bio text,
                contact_info text,
                contributions real
            )
        `);
    }

    /** Runs on rollback */
    async down(info: Info): Promise<void> {
        await this.client.queryObject(`
            DROP TABLE accounts
        `)
    }
}
