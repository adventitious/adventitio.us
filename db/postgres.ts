import { env } from "../env.ts";

console.log({ env });

const { Client } = await (async () => {
    console.log(env.RUNTIME);
    if (env.RUNTIME === "deno") {
        return await import("postgres");
    }
    return {} as any;
})();

export const clientOptions: import("postgres").ClientOptions = {
    applicationName: "adventitious",
    connection: {
        attempts: 1
    },
    database: "postgres",
    hostname: Deno.env.get("DB_HOST") ?? "localhost",
    password: "password",
    port: 5432,
    user: "postgres",
    tls: {
        enforce: false
    }
};

export const client = Client ? new Client(clientOptions) : undefined;

