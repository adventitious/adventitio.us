var __defProp = Object.defineProperty;
var __getOwnPropNames = Object.getOwnPropertyNames;
var __esm = (fn, res) => function __init() {
  return fn && (res = (0, fn[__getOwnPropNames(fn)[0]])(fn = 0)), res;
};
var __export = (target, all) => {
  for (var name in all)
    __defProp(target, name, { get: all[name], enumerable: true });
};

// http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/build.ts
var build;
var init_build = __esm({
  "http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/build.ts"() {
    build = {
      target: "unknown",
      arch: "unknown",
      os: "unknown",
      vendor: "unknown",
      env: void 0
    };
  }
});

// http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/internals.ts
function exposeForTest(name, value) {
  Object.defineProperty(internalObject, name, {
    value,
    enumerable: false
  });
}
var internalSymbol, internalObject;
var init_internals = __esm({
  "http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/internals.ts"() {
    internalSymbol = Symbol("Deno.internal");
    internalObject = {};
  }
});

// http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/util.ts
function assert(cond, msg = "Assertion failed.") {
  if (!cond) {
    throw new AssertionError(msg);
  }
}
function pathFromURLWin32(url) {
  if (url.hostname !== "") {
    return `\\\\${url.hostname}${url.pathname}`;
  }
  const validPath = /^\/(?<driveLetter>[A-Za-z]):\//;
  const matches = validPath.exec(url.pathname);
  if (!matches?.groups?.driveLetter) {
    throw new TypeError("A URL with the file schema must be absolute.");
  }
  const pathname = url.pathname.replace(/\//g, "\\");
  return pathname.slice(1);
}
function pathFromURLPosix(url) {
  if (url.hostname !== "") {
    throw new TypeError(`Host must be empty.`);
  }
  return decodeURIComponent(url.pathname);
}
function pathFromURL(pathOrUrl) {
  if (typeof pathOrUrl == "string") {
    try {
      pathOrUrl = new URL(pathOrUrl);
    } catch {
    }
  }
  if (pathOrUrl instanceof URL) {
    if (pathOrUrl.protocol != "file:") {
      throw new TypeError("Must be a path string or file URL.");
    }
    return build.os == "windows" ? pathFromURLWin32(pathOrUrl) : pathFromURLPosix(pathOrUrl);
  }
  return pathOrUrl;
}
var AssertionError;
var init_util = __esm({
  "http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/util.ts"() {
    init_build();
    init_internals();
    AssertionError = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "AssertionError";
      }
    };
    exposeForTest("pathFromURL", pathFromURL);
  }
});

// http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/buffer.ts
var buffer_exports = {};
__export(buffer_exports, {
  Buffer: () => Buffer2,
  readAll: () => readAll,
  readAllSync: () => readAllSync,
  writeAll: () => writeAll,
  writeAllSync: () => writeAllSync
});
function copyBytes(src, dst, off = 0) {
  const r = dst.byteLength - off;
  if (src.byteLength > r) {
    src = src.subarray(0, r);
  }
  dst.set(src, off);
  return src.byteLength;
}
async function readAll(r) {
  const buf = new Buffer2();
  await buf.readFrom(r);
  return buf.bytes();
}
function readAllSync(r) {
  const buf = new Buffer2();
  buf.readFromSync(r);
  return buf.bytes();
}
async function writeAll(w, arr) {
  let nwritten = 0;
  while (nwritten < arr.length) {
    nwritten += await w.write(arr.subarray(nwritten));
  }
}
function writeAllSync(w, arr) {
  let nwritten = 0;
  while (nwritten < arr.length) {
    nwritten += w.writeSync(arr.subarray(nwritten));
  }
}
var MIN_READ, MAX_SIZE, Buffer2;
var init_buffer = __esm({
  "http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/buffer.ts"() {
    init_util();
    MIN_READ = 512;
    MAX_SIZE = 2 ** 32 - 2;
    Buffer2 = class {
      #buf;
      #off = 0;
      constructor(ab) {
        if (ab == null) {
          this.#buf = new Uint8Array(0);
          return;
        }
        this.#buf = new Uint8Array(ab);
      }
      bytes() {
        return this.#buf.subarray(this.#off);
      }
      empty() {
        return this.#buf.byteLength <= this.#off;
      }
      get length() {
        return this.#buf.byteLength - this.#off;
      }
      get capacity() {
        return this.#buf.buffer.byteLength;
      }
      truncate(n) {
        if (n === 0) {
          this.reset();
          return;
        }
        if (n < 0 || n > this.length) {
          throw Error("bytes.Buffer: truncation out of range");
        }
        this.#reslice(this.#off + n);
      }
      reset() {
        this.#reslice(0);
        this.#off = 0;
      }
      #tryGrowByReslice = (n) => {
        const l = this.#buf.byteLength;
        if (n <= this.capacity - l) {
          this.#reslice(l + n);
          return l;
        }
        return -1;
      };
      #reslice = (len) => {
        assert(len <= this.#buf.buffer.byteLength);
        this.#buf = new Uint8Array(this.#buf.buffer, 0, len);
      };
      readSync(p) {
        if (this.empty()) {
          this.reset();
          if (p.byteLength === 0) {
            return 0;
          }
          return null;
        }
        const nread = copyBytes(this.#buf.subarray(this.#off), p);
        this.#off += nread;
        return nread;
      }
      read(p) {
        const rr = this.readSync(p);
        return Promise.resolve(rr);
      }
      writeSync(p) {
        const m = this.#grow(p.byteLength);
        return copyBytes(p, this.#buf, m);
      }
      write(p) {
        const n = this.writeSync(p);
        return Promise.resolve(n);
      }
      #grow = (n) => {
        const m = this.length;
        if (m === 0 && this.#off !== 0) {
          this.reset();
        }
        const i = this.#tryGrowByReslice(n);
        if (i >= 0) {
          return i;
        }
        const c = this.capacity;
        if (n <= Math.floor(c / 2) - m) {
          copyBytes(this.#buf.subarray(this.#off), this.#buf);
        } else if (c > MAX_SIZE - c - n) {
          throw new Error("The buffer cannot be grown beyond the maximum size.");
        } else {
          const buf = new Uint8Array(2 * c + n);
          copyBytes(this.#buf.subarray(this.#off), buf);
          this.#buf = buf;
        }
        this.#off = 0;
        this.#reslice(m + n);
        return m;
      };
      grow(n) {
        if (n < 0) {
          throw Error("Buffer.grow: negative count");
        }
        const m = this.#grow(n);
        this.#reslice(m);
      }
      async readFrom(r) {
        let n = 0;
        while (true) {
          const i = this.#grow(MIN_READ);
          this.#reslice(i);
          const fub = new Uint8Array(this.#buf.buffer, i);
          const nread = await r.read(fub);
          if (nread === null) {
            return n;
          }
          this.#reslice(i + nread);
          n += nread;
        }
      }
      readFromSync(r) {
        let n = 0;
        while (true) {
          const i = this.#grow(MIN_READ);
          this.#reslice(i);
          const fub = new Uint8Array(this.#buf.buffer, i);
          const nread = r.readSync(fub);
          if (nread === null) {
            return n;
          }
          this.#reslice(i + nread);
          n += nread;
        }
      }
    };
  }
});

// http-import:https://raw.githubusercontent.com/denoland/deno/v1.0.0/cli/js/io.ts
var io_exports = {};
__export(io_exports, {
  SeekMode: () => SeekMode,
  copy: () => copy,
  iter: () => iter,
  iterSync: () => iterSync
});
async function copy(src, dst, options) {
  let n = 0;
  const bufSize = options?.bufSize ?? DEFAULT_BUFFER_SIZE;
  const b = new Uint8Array(bufSize);
  let gotEOF = false;
  while (gotEOF === false) {
    const result = await src.read(b);
    if (result === null) {
      gotEOF = true;
    } else {
      let nwritten = 0;
      while (nwritten < result) {
        nwritten += await dst.write(b.subarray(nwritten, result));
      }
      n += nwritten;
    }
  }
  return n;
}
async function* iter(r, options) {
  const bufSize = options?.bufSize ?? DEFAULT_BUFFER_SIZE;
  const b = new Uint8Array(bufSize);
  while (true) {
    const result = await r.read(b);
    if (result === null) {
      break;
    }
    yield b.subarray(0, result);
  }
}
function* iterSync(r, options) {
  const bufSize = options?.bufSize ?? DEFAULT_BUFFER_SIZE;
  const b = new Uint8Array(bufSize);
  while (true) {
    const result = r.readSync(b);
    if (result === null) {
      break;
    }
    yield b.subarray(0, result);
  }
}
var DEFAULT_BUFFER_SIZE, SeekMode;
var init_io = __esm({
  "http-import:https://raw.githubusercontent.com/denoland/deno/v1.0.0/cli/js/io.ts"() {
    DEFAULT_BUFFER_SIZE = 32 * 1024;
    SeekMode = /* @__PURE__ */ ((SeekMode3) => {
      SeekMode3[SeekMode3["Start"] = 0] = "Start";
      SeekMode3[SeekMode3["Current"] = 1] = "Current";
      SeekMode3[SeekMode3["End"] = 2] = "End";
      return SeekMode3;
    })(SeekMode || {});
  }
});

// http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/errors.ts
var NotFound, PermissionDenied, ConnectionRefused, ConnectionReset, ConnectionAborted, NotConnected, AddrInUse, AddrNotAvailable, BrokenPipe, AlreadyExists, InvalidData, TimedOut, Interrupted, WriteZero, UnexpectedEof, BadResource, Http, Busy, errors;
var init_errors = __esm({
  "http-import:https://raw.githubusercontent.com/denoland/deno/v1.1.0/cli/js/errors.ts"() {
    NotFound = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "NotFound";
      }
    };
    PermissionDenied = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "PermissionDenied";
      }
    };
    ConnectionRefused = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "ConnectionRefused";
      }
    };
    ConnectionReset = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "ConnectionReset";
      }
    };
    ConnectionAborted = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "ConnectionAborted";
      }
    };
    NotConnected = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "NotConnected";
      }
    };
    AddrInUse = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "AddrInUse";
      }
    };
    AddrNotAvailable = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "AddrNotAvailable";
      }
    };
    BrokenPipe = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "BrokenPipe";
      }
    };
    AlreadyExists = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "AlreadyExists";
      }
    };
    InvalidData = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "InvalidData";
      }
    };
    TimedOut = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "TimedOut";
      }
    };
    Interrupted = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "Interrupted";
      }
    };
    WriteZero = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "WriteZero";
      }
    };
    UnexpectedEof = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "UnexpectedEof";
      }
    };
    BadResource = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "BadResource";
      }
    };
    Http = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "Http";
      }
    };
    Busy = class extends Error {
      constructor(msg) {
        super(msg);
        this.name = "Busy";
      }
    };
    errors = {
      NotFound,
      PermissionDenied,
      ConnectionRefused,
      ConnectionReset,
      ConnectionAborted,
      NotConnected,
      AddrInUse,
      AddrNotAvailable,
      BrokenPipe,
      AlreadyExists,
      InvalidData,
      TimedOut,
      Interrupted,
      WriteZero,
      UnexpectedEof,
      BadResource,
      Http,
      Busy
    };
  }
});

// http-import:https://deno.land/x/denofill/ops.ts
var ops_exports = {};
__export(ops_exports, {
  SeekMode: () => SeekMode2,
  close: () => close,
  copyFile: () => copyFile,
  errors: () => errors,
  getResources: () => getResources,
  open: () => open,
  purgeResources: () => purgeResources,
  read: () => read,
  readSync: () => readSync,
  seek: () => seek,
  seekSync: () => seekSync,
  truncate: () => truncate,
  write: () => write,
  writeSync: () => writeSync
});
function closeResource(rid) {
  if (resources.has(rid)) {
    resources.get(rid).closed = true;
    return;
  }
  throw new errors.BadResource(`Bad Resource: ${rid}`);
}
function copyResource(from, to) {
  if (!files.has(from)) {
    throw new errors.NotFound(`File does not exist: ${from}`);
  }
  const toRid = openResource(to, { write: true, create: true });
  const data = new Uint8Array(files.get(from).buf);
  writeResource(toRid, data);
  closeResource(toRid);
}
function getResources() {
  const result = {};
  for (const [key, value] of resources) {
    if (value.closed === false) {
      result[key] = value.name;
    }
  }
  return result;
}
function openResource(name, options) {
  const hasFile = files.has(name);
  if (hasFile) {
    if (options.createNew) {
      throw new errors.AlreadyExists(`File already exists: ${name}`);
    }
  }
  const rid = resourceId++;
  const resource = new Resource(name);
  resources.set(rid, resource);
  if (options.truncate) {
    new Uint8Array(resource.buf).set(new Uint8Array(0), 0);
  }
  resource.pos = options.append ? resource.buf.byteLength : 0;
  return rid;
}
function purgeResources() {
  const toDeleteResources = [];
  const toDeleteFiles = [];
  for (const [key, resource] of resources) {
    if (resource.closed) {
      toDeleteResources.push(key);
      if (!toDeleteFiles.includes(resource.name)) {
        toDeleteFiles.push(resource.name);
      }
    }
  }
  for (const key of toDeleteResources) {
    resources.delete(key);
  }
  for (const key of toDeleteFiles) {
    files.delete(key);
  }
}
function readResource(rid, data) {
  if (resources.has(rid)) {
    const item = resources.get(rid);
    if (item.closed) {
      throw new Error("rid closed");
    }
    if (item.options && !item.options.read) {
      throw new errors.PermissionDenied(`Resource not open for reading: ${rid}`);
    }
    const { pos } = item;
    const ab = new Uint8Array(item.buf);
    const remaining = ab.byteLength - pos;
    const readLength = remaining > data.byteLength ? data.byteLength : remaining;
    data.set(ab.slice(pos, pos + readLength), 0);
    item.pos += readLength;
    return readLength;
  }
  return -1;
}
function seekResource(rid, offset, whence) {
  if (resources.has(rid)) {
    const item = resources.get(rid);
    if (item.closed) {
      throw new errors.BadResource(`Resource is closed: ${rid}`);
    }
    const ua = new Uint8Array(item.buf);
    switch (whence) {
      case 1 /* Current */:
        item.pos = item.pos + ua.byteLength;
        break;
      case 2 /* End */:
        item.pos = ua.byteLength - offset;
        break;
      case 0 /* Start */:
        item.pos;
    }
    if (item.pos >= ua.byteLength) {
      item.pos = ua.byteLength - 1;
    } else if (item.pos < 0) {
      item.pos = 0;
    }
    return item.pos;
  }
  throw new errors.BadResource(`Bad Resource: ${rid}`);
}
function truncateResource(path, len = 0) {
  if (files.has(path)) {
    const item = files.get(path);
    const ab = new Uint8Array(item.buf);
    const nb = new Uint8Array(item.buf.slice(0, len));
    ab.set(nb, 0);
  } else {
    throw new errors.NotFound(`File not found: ${path}`);
  }
}
function writeResource(rid, data) {
  if (resources.has(rid)) {
    const item = resources.get(rid);
    if (item.closed) {
      throw new errors.BadResource(`Resource is closed: ${rid}`);
    }
    if (item.options && !item.options.write) {
      throw new errors.PermissionDenied(`Resource not open for writing: ${rid}`);
    }
    const ab = new Uint8Array(item.buf);
    const byteLength = data.byteLength + item.pos;
    const b = new Uint8Array(ab.byteLength > byteLength ? ab.byteLength : byteLength);
    b.set(ab, 0);
    b.set(data, item.pos);
    item.buf = b;
    item.pos += data.byteLength;
    return data.byteLength;
  }
  return -1;
}
function close(rid) {
  closeResource(rid);
}
function copyFile(fromPath, toPath) {
  copyResource(fromPath, toPath);
}
function open(path, options) {
  return openResource(path, options);
}
async function read(rid, buffer) {
  if (buffer.length === 0) {
    return Promise.resolve(0);
  }
  const nread = readResource(rid, buffer);
  if (nread < 0) {
    throw new Error("read error");
  } else if (nread === 0) {
    return null;
  } else {
    return nread;
  }
}
function readSync(rid, buffer) {
  if (buffer.length === 0) {
    return 0;
  }
  const nread = readResource(rid, buffer);
  if (nread < 0) {
    throw new Error("read error");
  } else if (nread === 0) {
    return null;
  } else {
    return nread;
  }
}
function seek(rid, offset, whence) {
  try {
    const result = seekResource(rid, offset, whence);
    return Promise.resolve(result);
  } catch (e) {
    return Promise.reject(e);
  }
}
function seekSync(rid, offset, whence) {
  return seekResource(rid, offset, whence);
}
function truncate(path, len) {
  truncateResource(path, len);
}
async function write(rid, data) {
  const result = writeResource(rid, data);
  if (result < 0) {
    throw new Error("write error");
  } else {
    return result;
  }
}
function writeSync(rid, data) {
  const result = writeResource(rid, data);
  if (result < 0) {
    throw new Error("write error");
  }
  return result;
}
var SeekMode2, Resource, files, resources, resourceId;
var init_ops = __esm({
  "http-import:https://deno.land/x/denofill/ops.ts"() {
    init_errors();
    init_errors();
    SeekMode2 = /* @__PURE__ */ ((SeekMode3) => {
      SeekMode3[SeekMode3["Start"] = 0] = "Start";
      SeekMode3[SeekMode3["Current"] = 1] = "Current";
      SeekMode3[SeekMode3["End"] = 2] = "End";
      return SeekMode3;
    })(SeekMode2 || {});
    Resource = class {
      constructor(name) {
        this.pos = 0;
        this.closed = false;
        this.name = name;
      }
      get buf() {
        if (files.has(this.name)) {
          return files.get(this.name).buf;
        }
        const buf = new ArrayBuffer(0);
        files.set(this.name, { buf });
        return buf;
      }
      set buf(buf) {
        if (files.has(this.name)) {
          files.get(this.name).buf = buf;
        } else {
          files.set(this.name, { buf });
        }
      }
    };
    files = /* @__PURE__ */ new Map();
    resources = /* @__PURE__ */ new Map();
    resourceId = 0;
  }
});

// http-import:https://deno.land/x/denofill/mod.ts
var denoProperties;
var purge;
function readOnly(value) {
  return {
    value,
    writable: false,
    enumerable: true,
    configurable: false
  };
}
function noop() {
}
async function asyncNoop() {
}
function* genNoop() {
}
async function* asyncGenNoop() {
}
function notImplemented() {
  throw new Error("This feature is not implemented in browsers.");
}
async function init() {
  if (window && !("Deno" in window)) {
    Object.defineProperty(window, "Deno", {
      value: await getPolyfill(),
      writable: false,
      enumerable: true,
      configurable: true
    });
  }
}
async function getPolyfill() {
  if (denoProperties) {
    const shim2 = /* @__PURE__ */ Object.create(null);
    Object.defineProperties(shim2, denoProperties);
    Object.freeze(shim2);
    return shim2;
  }
  const { Buffer: Buffer3, readAll: readAll2, readAllSync: readAllSync2, writeAll: writeAll2, writeAllSync: writeAllSync2 } = await Promise.resolve().then(() => (init_buffer(), buffer_exports));
  const { copy: copy2, iter: iter2, iterSync: iterSync2 } = await Promise.resolve().then(() => (init_io(), io_exports));
  const {
    errors: errors2,
    close: close2,
    copyFile: opCopyFile,
    getResources: resources2,
    open: opOpen,
    purgeResources: purgeResources2,
    read: read2,
    readSync: readSync2,
    seek: seek2,
    SeekMode: SeekMode3,
    seekSync: seekSync2,
    truncate: opTruncate,
    write: write2,
    writeSync: writeSync2
  } = await Promise.resolve().then(() => (init_ops(), ops_exports));
  purge = purgeResources2;
  function exit() {
    window && window.close();
  }
  class Env {
    #env = {};
    delete(key) {
      delete this.#env[key];
    }
    get(key) {
      return this.#env[key];
    }
    set(key, value) {
      this.#env[key] = value;
    }
    toObject() {
      return { ...this.#env };
    }
  }
  const env = new Env();
  function cwd() {
    if (window && window.location) {
      const loc = window.location;
      return `${loc.origin}${loc.pathname}`;
    }
    return "";
  }
  class File {
    constructor(rid) {
      this.rid = rid;
    }
    write(p) {
      return write2(this.rid, p);
    }
    writeSync(p) {
      return writeSync2(this.rid, p);
    }
    read(p) {
      return read2(this.rid, p);
    }
    readSync(p) {
      return readSync2(this.rid, p);
    }
    seek(offset, whence) {
      return seek2(this.rid, offset, whence);
    }
    seekSync(offset, whence) {
      return seekSync2(this.rid, offset, whence);
    }
    close() {
      close2(this.rid);
    }
  }
  function checkOpenOptions(options) {
    if (!Object.values(options).some((val) => val === true)) {
      throw new Error("OpenOptions requires at least one option to be true");
    }
    if (options.truncate && !options.write) {
      throw new Error("'truncate' option requires 'write' option");
    }
    const createOrCreateNewWithoutWriteOrAppend = (options.create || options.createNew) && !(options.write || options.append);
    if (createOrCreateNewWithoutWriteOrAppend) {
      throw new Error("'create' or 'createNew' options require 'write' or 'append' option");
    }
  }
  function open2(path, options = { read: true }) {
    try {
      checkOpenOptions(options);
      const rid = opOpen(path, options);
      return Promise.resolve(new File(rid));
    } catch (e) {
      return Promise.reject(e);
    }
  }
  function openSync(path, options = { read: true }) {
    checkOpenOptions(options);
    const rid = opOpen(path, options);
    return new File(rid);
  }
  function create(path) {
    return open2(path, {
      read: true,
      write: true,
      truncate: true,
      create: true
    });
  }
  function createSync(path) {
    return openSync(path, {
      read: true,
      write: true,
      truncate: true,
      create: true
    });
  }
  class Stdin {
    constructor() {
      this.rid = opOpen("/dev/stdin", { read: true });
    }
    read(p) {
      return read2(this.rid, p);
    }
    readSync(p) {
      return readSync2(this.rid, p);
    }
    close() {
      close2(this.rid);
    }
  }
  class Stdout {
    constructor() {
      this.rid = opOpen("/dev/stdout", { write: true });
    }
    write(p) {
      return write2(this.rid, p);
    }
    writeSync(p) {
      return writeSync2(this.rid, p);
    }
    close() {
      close2(this.rid);
    }
  }
  class Stderr {
    constructor() {
      this.rid = opOpen("/dev/stderr", { write: true });
    }
    write(p) {
      return write2(this.rid, p);
    }
    writeSync(p) {
      return writeSync2(this.rid, p);
    }
    close() {
      close2(this.rid);
    }
  }
  const stdin = new Stdin();
  const stdout = new Stdout();
  const stderr = new Stderr();
  function isatty() {
    return false;
  }
  function makeTempDirSync({
    dir = "/tmp",
    prefix = "",
    suffix = ""
  } = {}) {
    const str = Math.random().toString(36).substring(7);
    return `${dir}${dir.match(/\/$/) ? "" : "/"}${prefix}${str}${suffix}`;
  }
  function makeTempDir(options) {
    return Promise.resolve(makeTempDirSync(options));
  }
  function makeTempFileSync(options) {
    const str = makeTempDirSync(options);
    close2(opOpen(str, { read: true }));
    return str;
  }
  function makeTempFile(options) {
    return Promise.resolve(makeTempFileSync(options));
  }
  const decoder = new TextDecoder();
  function readTextFileSync(path) {
    const file = openSync(path);
    const content = readAllSync2(file);
    file.close();
    return decoder.decode(content);
  }
  function readTextFile(path) {
    return Promise.resolve(readTextFileSync(path));
  }
  function readFileSync(path) {
    const file = openSync(path);
    const content = readAllSync2(file);
    file.close();
    return content;
  }
  function readFile(path) {
    return Promise.resolve(readFileSync(path));
  }
  function realPathSync(path) {
    return path;
  }
  function realPath(path) {
    return Promise.resolve(realPathSync(path));
  }
  function copyFileSync(fromPath, toPath) {
    opCopyFile(fromPath, toPath);
  }
  async function copyFile2(fromPath, toPath) {
    copyFileSync(fromPath, toPath);
  }
  function writeFileSync(path, data, options) {
    const openOptions = !!options.append ? { write: true, create: true, append: true } : { write: true, create: true, truncate: true };
    const file = openSync(path, openOptions);
    writeAllSync2(file, data);
    file.close();
  }
  async function writeFile(path, data, options) {
    writeFileSync(path, data, options);
  }
  const encoder = new TextEncoder();
  function writeTextFileSync(path, data) {
    const file = openSync(path, { write: true, create: true, truncate: true });
    const contents = encoder.encode(data);
    writeAllSync2(file, contents);
    file.close();
  }
  async function writeTextFile(path, data) {
    writeTextFileSync(path, data);
  }
  function truncateSync(path, len) {
    opTruncate(path, len);
  }
  async function truncate2(path, len) {
    opTruncate(path, len);
  }
  function metrics() {
    return {
      opsDispatched: 0,
      opsDispatchedSync: 0,
      opsDispatchedAsync: 0,
      opsDispatchedAsyncUnref: 0,
      opsCompleted: 0,
      opsCompletedSync: 0,
      opsCompletedAsync: 0,
      opsCompletedAsyncUnref: 0,
      bytesSentControl: 0,
      bytesSentData: 0,
      bytesReceived: 0
    };
  }
  const build2 = {
    target: "browser",
    arch: "x86_64",
    os: "browser",
    vendor: "browser"
  };
  Object.freeze(build2);
  const version = {
    deno: "0.0.0",
    typescript: "0.0.0",
    v8: "0.0.0"
  };
  Object.freeze(build2);
  const customInspect = Symbol.for("custom inspect");
  denoProperties = {
    errors: readOnly(errors2),
    pid: readOnly(0),
    noColor: readOnly(true),
    test: readOnly(noop),
    exit: readOnly(exit),
    env: readOnly(env),
    execPath: readOnly(() => "/usr/bin/deno"),
    chdir: readOnly(noop),
    cwd: readOnly(cwd),
    SeekMode: readOnly(SeekMode3),
    copy: readOnly(copy2),
    iter: readOnly(iter2),
    iterSync: readOnly(iterSync2),
    open: readOnly(open2),
    openSync: readOnly(openSync),
    create: readOnly(create),
    createSync: readOnly(createSync),
    read: readOnly(read2),
    readSync: readOnly(readSync2),
    write: readOnly(write2),
    writeSync: readOnly(writeSync2),
    seek: readOnly(seek2),
    seekSync: readOnly(seekSync2),
    close: readOnly(close2),
    File: readOnly(File),
    stdin: readOnly(stdin),
    stdout: readOnly(stdout),
    stderr: readOnly(stderr),
    isatty: readOnly(isatty),
    Buffer: readOnly(Buffer3),
    readAll: readOnly(readAll2),
    readAllSync: readOnly(readAllSync2),
    writeAll: readOnly(writeAll2),
    writeAllSync: readOnly(writeAllSync2),
    mkdirSync: readOnly(noop),
    mkdir: readOnly(asyncNoop),
    makeTempDirSync: readOnly(makeTempDirSync),
    makeTempDir: readOnly(makeTempDir),
    makeTempFileSync: readOnly(makeTempFileSync),
    makeTempFile: readOnly(makeTempFile),
    chmodSync: readOnly(noop),
    chmod: readOnly(asyncNoop),
    chownSync: readOnly(noop),
    chown: readOnly(asyncNoop),
    removeSync: readOnly(noop),
    remove: readOnly(asyncNoop),
    renameSync: readOnly(noop),
    rename: readOnly(asyncNoop),
    readTextFileSync: readOnly(readTextFileSync),
    readTextFile: readOnly(readTextFile),
    readFileSync: readOnly(readFileSync),
    readFile: readOnly(readFile),
    realPathSync: readOnly(realPathSync),
    realPath: readOnly(realPath),
    readDirSync: readOnly(genNoop),
    readDir: readOnly(asyncGenNoop),
    copyFileSync: readOnly(copyFileSync),
    copyFile: readOnly(copyFile2),
    readLinkSync: readOnly(notImplemented),
    readLink: readOnly(notImplemented),
    lstat: readOnly(notImplemented),
    lstatSync: readOnly(notImplemented),
    stat: readOnly(notImplemented),
    statSync: readOnly(notImplemented),
    writeFileSync: readOnly(writeFileSync),
    writeFile: readOnly(writeFile),
    writeTextFileSync: readOnly(writeTextFileSync),
    writeTextFile: readOnly(writeTextFile),
    truncateSync: readOnly(truncateSync),
    truncate: readOnly(truncate2),
    listen: readOnly(notImplemented),
    listenTls: readOnly(notImplemented),
    connect: readOnly(notImplemented),
    connectTls: readOnly(notImplemented),
    metrics: readOnly(metrics),
    resources: readOnly(resources2),
    watchFs: readOnly(asyncGenNoop),
    Process: readOnly(notImplemented),
    run: readOnly(notImplemented),
    inspect: readOnly(notImplemented),
    build: readOnly(build2),
    version: readOnly(version),
    args: readOnly([]),
    customInspect: readOnly(customInspect),
    internal: readOnly(Symbol.for("Deno internal")),
    core: readOnly({})
  };
  const shim = /* @__PURE__ */ Object.create(null);
  Object.defineProperties(shim, denoProperties);
  Object.freeze(shim);
  return shim;
}

// web/init.ts
await init();
