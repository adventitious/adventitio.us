import {
    ClientPostgreSQL,
    NessieConfig
} from "nessie";

import { clientOptions } from "./db/postgres.ts";


export default {
    client: new ClientPostgreSQL(clientOptions),
    migrationFolders: ["./db/migrations"],
    seedFolders: ["./db/seeds"]
};

