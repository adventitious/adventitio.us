const isDeno = (() => {
    if (typeof Deno === "object") {
        return true;
    }
    return false;
})();

export const env = {
    RUNTIME: isDeno ? "deno" : "browser"
};

