// Auto-generated with deno_bindgen
import { CachePolicy, prepare } from "https://deno.land/x/plug@0.5.1/plug.ts"
function encode(v: string | Uint8Array): Uint8Array {
  if (typeof v !== "string") return v
  return new TextEncoder().encode(v)
}
function decode(v: Uint8Array): string {
  return new TextDecoder().decode(v)
}
function readPointer(v: any): Uint8Array {
  const ptr = new Deno.UnsafePointerView(v as Deno.UnsafePointer)
  const lengthBe = new Uint8Array(4)
  const view = new DataView(lengthBe.buffer)
  ptr.copyInto(lengthBe, 0)
  const buf = new Uint8Array(view.getUint32(0))
  ptr.copyInto(buf, 4)
  return buf
}
const opts = {
  name: "libatrial",
  url: (new URL("../target/debug", import.meta.url)).toString(),
  policy: CachePolicy.NONE,
}
const _lib = await prepare(opts, {
  initialize: { parameters: [], result: "void", nonblocking: false },
  login: {
    parameters: ["pointer", "usize", "pointer", "usize", "pointer", "usize"],
    result: "u8",
    nonblocking: false,
  },
})

export function initialize() {
  let rawResult = _lib.symbols.initialize()
  const result = rawResult
  return result
}
export function login(a0: string, a1: string, a2: string) {
  const a0_buf = encode(a0)
  const a1_buf = encode(a1)
  const a2_buf = encode(a2)
  let rawResult = _lib.symbols.login(
    a0_buf,
    a0_buf.byteLength,
    a1_buf,
    a1_buf.byteLength,
    a2_buf,
    a2_buf.byteLength,
  )
  const result = rawResult
  return result
}
