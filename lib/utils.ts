import delay from "delay";


// Retry
//
// Retries a function every second until it succeeds
// TODO add parameters that allow contorl of the number of retries and the delay,
// as well as optionally a backoff function to have the delay change based on the 
// number of retries that have occured.
//
export const retry = (fn: any) => async (...args: any[]) => {
    let shouldRetry = true;
    while (shouldRetry) {
        try {
            return fn(...args);
        } catch (err) {
            await delay(1000);
        }
    }
};


// Unbound Getter Builder
//
// Basically, accessing a property returns an object whose Symbol getter 
// is a function that takes an object and returns that property, further 
// access of other properties on that object does the same thing. 
//
export const getter = Symbol();
export const argsGetHandler = (target: any, prop: any, receiver: any): any => {
    if (prop === getter) {
        return target[getter]        
    }
    return new Proxy({
        [getter]: (context: any) => target[getter](context)[prop]
    }, {
        get: argsGetHandler
    })
};
// TODO args doesnt work as is, needs to be a proxy, or put a default on initial target.
export const get: any = new Proxy({
    [getter]: (context: any) => context
}, {
    get: argsGetHandler
});


// Template Compiler Tagged Literal
//
// Tagged template returns a function that can be called with a contextx, which is passed into
// the unbound getters that are included in the ${} portions of the string.
//
// Depends on the pregious getter function, or similar.
//
export const template = (stringParts: string[], ...getters: any[]) => (context: any) => {
    return stringParts.reduce((built: string, stringPart: string) => {
         return `${built}${stringPart}${getters.pop()(context)}`
    }, "");
};

export const This = {};

export const validateIp = (ip: string) => {
    return ip.split(".").every((subpart) => {
        if (typeof subpart !== "number") {
            return false;
        }
        if (subpart < 0) {
            return false;
        }
        if (subpart > 255) {
            return false;
        }
        return true;
    })
};
