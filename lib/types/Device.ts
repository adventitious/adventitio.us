import { nanoid } from "nanoid";
import { pluralize } from "inflected";

import { env } from "../../env.ts";

import type { Client } from "postgres";

const { client: postgres }: { client: Client } = await (async () => {
    if (env.RUNTIME === "deno") {
        return await import("../../db/postgres.ts");
    }
    return { client: undefined as unknown as Client };
})();

import { 
    This,
    get,
    validateIp
} from "../utils.ts";

import {
    OperationSignatureContext,
    BaseType,
    query,
    mutation,
    field,
    args
} from "./Base.ts";


const GET_DEVICE = "SELECT * FROM nodes WHERE id = $id";

export class Device extends BaseType {

    static ownFields = new Map();
    static ownQueries = new Map();
    static ownMutations = new Map();
    static ownSubscriptions = new Map();

    _id: symbol;
    _ip: string;
    _publicKey: string;

    @field(Symbol)
    get id() {
        return Symbol.keyFor(this._id);
    };

    @field(String)
    get ip() {
        return this._ip;
    };

    @field(String)
    get publicKey() {
        return this._publicKey;
    };

    constructor({ id, ip, publicKey }: { id: symbol; ip: string; publicKey: string }) {
        super();
        this._id = id;
        this._ip = ip;
        this._publicKey = publicKey;
    }

    static from(data: any) {
        let id;
        let publicKey;

        if (typeof data.id === "string") {
            id = Symbol.for(data.id);
        }

        if (typeof data.id === "symbol") {
            if (typeof Symbol.keyFor(data.id) === "undefined") {
                throw new Error("Symbol used as id has no key");
            }
            id = data.id;
        }

        if (!validateIp(data.ip)) {
            console.log("Got a bad IP");
            //throw new Error("Provided ip had a subpart that wasnt a number between 0 and 255");
        }

        const ip = data.ip as string; // ip fails validation if its not a string

        return new this({
            id,
            ip,
            publicKey: data.publicKey
        });
    }

    @query({
        operationName: ({ receiver }) => `all${pluralize(receiver.name)}`,
        parameters: () => ({
            limit: BigInt,
            offset: BigInt
        }),
        returns: () => []
    })
    static async all(
        @args({
            limit: ({ args }) => args.limit,
            offset: ({ args }) => args.offset
        }) {
            limit, 
            offset 
        }: any
    ) {
        if (env.RUNTIME === "deno") {
            const result = await postgres.queryObject(`
                SELECT * FROM devices
            `, { limit, offset });
            return result.rows.map((row: any) => this.from(row));
        }
        return [];
    }

    @mutation({
        operationName: ({ receiver }) => `register${receiver.name}`,
        parameters: () => ({
            ip: String,
            name: String,
            publicKey: String    
        }),
        returns: () => ({})
    })
    static async register(
        @args({
            name: (rawr) => {
                console.log({rawr});
                return rawr.args.name;
            },
            ip: (rawr) => {
                console.log(rawr);
                return rawr.args.ip;
            },
            publicKey: ({ args }) => args.publicKey
        }) {
            name,
            ip,
            publicKey 
        }: {
            name: string;
            ip: string;
            publicKey: string;
        }
    ) {
        if (env.RUNTIME === "deno") {
            const id = nanoid();
            const data = { id, name, ip, publicKey };
            console.log(data);
            return await postgres.queryObject(`
                INSERT INTO devices (
                    id,
                    name,
                    ip,
                    public_key
                ) VALUES (
                    $id,
                    $name,
                    $ip,
                    $publicKey
                )
            `, data);
        }
        return;
    }

}

