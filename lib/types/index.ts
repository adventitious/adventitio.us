import { Activity } from "./Activity.ts";
import { Capability } from "./Capability.ts";
import { Request } from "./Request.ts";
import { Device } from "./Device.ts";


export const types = [
    Activity,
    Capability,
    Request,
    Device
];

