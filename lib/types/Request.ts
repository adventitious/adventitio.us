import { nanoid } from "nanoid";
import { pluralize } from "inflected";

import { Activity } from "./Activity.ts";

import { env } from "../../env.ts";

import type { Client } from "postgres";

const { client: postgres }: { client: Client } = await (async () => {
    if (env.RUNTIME === "deno") {
        return await import("../../db/postgres.ts");
    }
    return { client: undefined as unknown as Client };
})();

import { 
    This,
    get,
    validateIp
} from "../utils.ts";

import {
    OperationSignatureContext,
    BaseType,
    query,
    mutation,
    field,
    args
} from "./Base.ts";


const GET_DEVICE = "SELECT * FROM nodes WHERE id = $id";

export class Request extends BaseType {

    static ownFields = new Map();
    static ownQueries = new Map();
    static ownMutations = new Map();
    static ownSubscriptions = new Map();

    _id: symbol;
    _title: string;
    _description: string;
    _contactInfo: string;
    _withdrawn: string;
    _activityId: symbol;
    _requestorId: symbol;

    @field(Symbol)
    get id() {
        return Symbol.keyFor(this._id);
    };

    @field(String)
    get title() {
        return this._title;
    };

    @field(String)
    get description() {
        return this._description;
    };

    @field(String)
    get contactInfo() {
        return this._contactInfo;
    };

    @field(Boolean)
    get withdrawn() {
        return this._withdrawn;
    };

    @field(Symbol)
    get activityId() {
        if (!this._activityId) {
            return null;
        }
        return Symbol.keyFor(this._activityId);
    };

    @field(Activity)
    get activity() {
        if (!this._activityId) {
            return null;
        }
        return Activity.findOne({
            id: Symbol.keyFor(this._activityId)
        });
    };

    @field(Symbol)
    get requestorId() {
        if (!this._requestorId) {
            return null;
        }
        return Symbol.keyFor(this._requestorId);
    };

    @field(Activity)
    get requestor() {
        if (!this._requestorId) {
            return null;
        }
        return Activity.findOne({
            id: Symbol.keyFor(this._requestorId)
        });
    };

    constructor({
        id,
        title,
        description,
        contactInfo,
        withdrawn,
        activityId,
	requestorId
    }: {
        id: symbol;
        title: string;
        description: string;
        contactInfo: string;
        withdrawn: boolean;
        activityId: symbol;
	requestorId: symbol;
    }) {
        super();
        this._id = id;
        this._title = title;
        this._description = description;
        this._contactInfo = contactInfo;
        this._withdrawn = withdrawn;
        this._activityId = activityId;
        this._requestorId = requestorId;
    }

    static from(data: any) {
        let id;
        let activityId;
        let requestorId;
        let publicKey;

        console.log(data);
        if (typeof data.id === "string") {
            id = Symbol.for(data.id);
        }

        if (typeof data.id === "symbol") {
            if (typeof Symbol.keyFor(data.id) === "undefined") {
                throw new Error("Symbol used as id has no key");
            }
            id = data.id;
        }

        if (typeof data.activity_id === "symbol") {
            if (typeof Symbol.keyFor(data.activity_id) === "undefined") {
                throw new Error("Symbol used as id has no key");
            }
            activityId = data.activity_id;
        }

        if (typeof data.activity_id === "string") {
            activityId = Symbol.for(data.activity_id)
        }

	if (typeof data.requestor_id === "symbol") {
            if (typeof Symbol.keyFor(data.requestor_id) === "undefined") {
                throw new Error("Symbol used as requestor_id has no key")	
            }
	    requestorId = data.requestor_id;
	}

        return new this({
            id,
            title: data.title,
            description: data.description,
            contactInfo: data.contact_info,
            withdrawn: data.withdrawn,
            activityId,
	    requestorId
        });
    }

    @query({
        operationName: ({ receiver }) => `find${receiver.name}`,
        parameters: () => ({
            id: Symbol
        }),
        returns: () => ({})
    })
    static async findOne(
        @args({
            id: ({ args }) => args.id
        }) {
            id 
        }: any
    ) {
        if (env.RUNTIME === "deno") {
            const result = await postgres.queryObject(`
                SELECT * FROM requests WHERE id = $id
            `, { id });
            return result.rows.map((row: any) => this.from(row))[0];
        }
        return null;
    }

    @query({
        operationName: ({ receiver }) => `all${pluralize(receiver.name)}`,
        parameters: () => ({
            limit: BigInt,
            offset: BigInt
        }),
        returns: () => []
    })
    static async all(
        @args({
            limit: ({ args }) => args.limit,
            offset: ({ args }) => args.offset
        }) {
            limit, 
            offset 
        }: any
    ) {
        if (env.RUNTIME === "deno") {
            const result = await postgres.queryObject(`
                SELECT * FROM requests
            `, { limit, offset });
            return result.rows.map((row: any) => this.from(row));
        }
        return [];
    }

    @mutation({
        operationName: ({ receiver }) => `create${receiver.name}`,
        parameters: () => ({
            title: String,
            description: String,
            contactInfo: String,
            activity: Symbol
        }),
        returns: () => ({})
    })
    static async create(
        @args({
            title: ({ args }) => args.title,
            description: ({ args }) => args.description,
            contactInfo: ({ args }) => args.contactInfo,
            activity: ({ args }) => args.activity
        }) {
            title,
            description,
            contactInfo,
            activity
        }: {
            title: string;
            description: string;
            contactInfo: string;
            activity: string;
        }
    ) {
        if (env.RUNTIME === "deno") {
            const id = nanoid();
            return (await postgres.queryObject`
                INSERT INTO requests (
                    id,
                    title,
                    description,
                    contact_info,
                    activity_id,
                    withdrawn
                ) VALUES (
                    ${id},
                    ${title},
                    ${description},
                    ${contactInfo},
                    ${activity},
                    ${false}
                ) RETURNING id
            `).rows[0];
        }
        return;
    }

    @mutation({
        operationName: ({ receiver }) => `withdraw${receiver.name}`,
        parameters: () => ({
            id: Symbol,
        }),
        returns: () => (Boolean)
    })
    static async withdraw(
        @args({
            id: ({ args }) => args.id
        }) {
            id
        }: any
    ) {
        if (env.RUNTIME === "deno") {
            await postgres.queryObject(`
                UPDATE requests
                SET
                    withdrawn = true
                WHERE
                    id = $id
            `, { id });
            return true;
        }
    }

}

