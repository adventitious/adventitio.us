import { nanoid } from "nanoid";
import { pluralize } from "inflected";


import { env } from "../../env.ts";

import type { Client } from "postgres";

const { client: postgres }: { client: Client } = await (async () => {
    if (env.RUNTIME === "deno") {
        return await import("../../db/postgres.ts");
    }
    return { client: undefined as unknown as Client };
})();

import { 
    This,
    get,
    validateIp
} from "../utils.ts";

import {
    OperationSignatureContext,
    BaseType,
    query,
    mutation,
    field,
    args
} from "./Base.ts";


export class Activity extends BaseType {

    static ownFields = new Map();
    static ownQueries = new Map();
    static ownMutations = new Map();
    static ownSubscriptions = new Map();

    _id: symbol;
    _title: string;
    _description: string;
    _demand: string;

    @field(Symbol)
    get id() {
        return Symbol.keyFor(this._id);
    };

    @field(String)
    get title() {
        return this._title;
    };

    @field(String)
    get description() {
        return this._description;
    };

    @field(BigInt)
    get demand() {
        return this._demand;
    };

    constructor({
        id,
        title,
        description,
        demand
    }: {
        id: symbol;
        title: string;
        description: string;
        demand: string;
    }) {
        super();
        this._id = id;
        this._title = title;
        this._description = description;
        this._demand = demand;
    }

    static from(data: any) {
        let id;
        let publicKey;

        if (typeof data.id === "string") {
            id = Symbol.for(data.id);
        }

        if (typeof data.id === "symbol") {
            if (typeof Symbol.keyFor(data.id) === "undefined") {
                throw new Error("Symbol used as id has no key");
            }
            id = data.id;
        }

        return new this({
            id,
            title: data.title,
            description: data.description,
        });
    }

    @query({
        operationName: ({ receiver }) => `find${receiver.name}`,
        parameters: () => ({
            id: Symbol
        }),
        returns: () => ({})
    })
    static async findOne(
        @args({
            id: ({ args }) => args.id
        }) {
            id 
        }: any
    ) {
        if (env.RUNTIME === "deno") {
            const result = await postgres.queryObject(`
                SELECT * FROM activities WHERE id = $id
            `, { id });
            return result.rows.map((row: any) => this.from(row))[0];
        }
        return null;
    }

    @query({
        operationName: ({ receiver }) => `all${pluralize(receiver.name)}`,
        parameters: () => ({
            title: String,
            limit: BigInt,
            offset: BigInt
        }),
        returns: () => []
    })
    static async all(
        @args({
            title: ({ args }) => args?.title ?? "_",
            limit: ({ args }) => args.limit,
            offset: ({ args }) => args.offset
        }) {
            title,
            limit, 
            offset 
        }: any
    ) {
        console.log({
            title,
            limit,
            offset
        });
        if (env.RUNTIME === "deno") {
            console.log(env.RUNTIME);
            const titleLike = `${title}%`;
            const result = await postgres.queryObject`
                SELECT * FROM activities
                WHERE title LIKE ${titleLike}
            `;
            console.log({
                result
            });
            return result.rows.map((row: any) => this.from(row));
        }
        return [];
    }

    @mutation({
        operationName: ({ receiver }) => `create${receiver.name}`,
        parameters: () => ({
            title: String,
            description: String,
        }),
        returns: () => ({})
    })
    static async create(
        @args({
            title: ({ args }) => args.title,
            description: ({ args }) => args.description,
        }) {
            title,
            description,
        }: {
            title: string;
            description: string;
        }
    ) {
        if (env.RUNTIME === "deno") {
            const id = nanoid();
            const data = {
                id,
                title,
                description,
                demand: 0
            };
            return (await postgres.queryObject(`
                INSERT INTO activities (
                    id,
                    title,
                    description,
                    demand
                ) VALUES (
                    $id,
                    $title,
                    $description,
                    $demand
                ) RETURNING id
            `, data)).rows[0];
        }
        return;
    }

    

}

