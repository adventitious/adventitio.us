import { nanoid } from "nanoid";
import { pluralize } from "inflected";


import { env } from "../../env.ts";

import type { Client } from "postgres";

const { client: postgres }: { client: Client } = await (async () => {
    if (env.RUNTIME === "deno") {
        return await import("../../db/postgres.ts");
    }
    return { client: undefined as unknown as Client };
})();

import { 
    This,
    get,
    validateIp
} from "../utils.ts";

import {
    OperationSignatureContext,
    BaseType,
    query,
    mutation,
    field,
    args
} from "./Base.ts";


export class Account extends BaseType {

    static ownFields = new Map();
    static ownQueries = new Map();
    static ownMutations = new Map();
    static ownSubscriptions = new Map();

    _id: symbol;
    _name: string;
    _bio: string;
    _contactInfo: string;
    _contributions: number;

    @field(Symbol)
    get id() {
        return Symbol.keyFor(this._id);
    };

    @field(String)
    get title() {
        return this._title;
    };

    @field(String)
    get description() {
        return this._description;
    };

    @field(String)
    get contactInfo() {
        return this._contactInfo;
    };

    @field(Number)
    get contributions() {
        return this._contributions;
    };

    constructor({
        id,
        title,
        description,
        contributions,
        contactInfo
    }: {
        id: symbol;
        title: string;
        description: string;
        contributions: number;
        contactInfo: string;
    }) {
        super();
        this._id = id;
        this._title = title;
        this._description = description;
        this._contributions = contributions;
        this._contactInfo = contactInfo;
    }

    static from(data: any) {
        let id;
        let publicKey;

        if (typeof data.id === "string") {
            id = Symbol.for(data.id);
        }

        if (typeof data.id === "symbol") {
            if (typeof Symbol.keyFor(data.id) === "undefined") {
                throw new Error("Symbol used as id has no key");
            }
            id = data.id;
        }

        return new this({
            id,
            title: data.title,
            description: data.description,
            contributions: data.contributions,
            contactInfo: data.contactInfo
        });
    }

    @query({
        operationName: ({ receiver }) => `find${receiver.name}`,
        parameters: () => ({
            id: Symbol
        }),
        returns: () => ({})
    })
    static async findOne(
        @args({
            id: ({ args }) => args.id
        }) {
            id 
        }: any
    ) {
        if (env.RUNTIME === "deno") {
            const result = await postgres.queryObject(`
                SELECT * FROM accounts WHERE id = $id
            `, { id });
            return result.rows.map((row: any) => this.from(row))[0];
        }
        return null;
    }

    @query({
        operationName: ({ receiver }) => `all${pluralize(receiver.name)}`,
        parameters: () => ({
            limit: BigInt,
            offset: BigInt
        }),
        returns: () => []
    })
    static async all(
        @args({
            limit: ({ args }) => args.limit,
            offset: ({ args }) => args.offset
        }) {
            limit, 
            offset 
        }: any
    ) {
        if (env.RUNTIME === "deno") {
            const result = await postgres.queryObject(`
                SELECT * FROM accounts
            `, { limit, offset });
            return result.rows.map((row: any) => this.from(row));
        }
        return [];
    }

    @mutation({
        operationName: ({ receiver }) => `create${receiver.name}`,
        parameters: () => ({
            title: String,
            description: String,
            contactInfo: String    
        }),
        returns: () => ({})
    })
    static async create(
        @args({
            title: ({ args }) => args.title,
            description: ({ args }) => args.description,
            contactInfo: ({ args }) => args.contactInfo
        }) {
            title,
            description,
            contactInfo 
        }: {
            title: string;
            description: string;
            contactInfo: string;
        }
    ) {
        if (env.RUNTIME === "deno") {
            const id = nanoid();
            const data = {
                id,
                title,
                description,
                contactInfo,
                contributions: 0
            };
            return (await postgres.queryObject(`
                INSERT INTO capabilities (
                    id,
                    title,
                    description,
                    contact_info,
                    contributions
                ) VALUES (
                    $id,
                    $title,
                    $description,
                    $contactInfo,
                    $contributions
                ) RETURNING id
            `, data)).rows[0];
        }
        return;
    }

}

