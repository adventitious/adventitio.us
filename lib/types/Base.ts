import { 
    Thunk,
    GraphQLInt,
    GraphQLString,
    GraphQLFloat,
    GraphQLBoolean,
    GraphQLID,
    GraphQLObjectType,
    GraphQLList,
    GraphQLFieldConfigMap,
} from "graphql";
import { camelize, pluralize } from "inflected";

import { get, getter, This } from "../utils.ts";

const typeMap = {
    "Symbol": GraphQLID,
    "Boolean": GraphQLBoolean,
    "Number": GraphQLFloat,
    "BigInt": GraphQLInt,
    "String": GraphQLString
};

export const field = (
    fieldType: any
) => (
    object: any,
    property: any,
    descriptor: any
) => {
    console.log(fieldType);
    console.log(fieldType.name);
    descriptor.type = (typeMap as Record<string, any>)[fieldType.name as string] ?? fieldType.type;
    const resolverHandler = descriptor?.get ?? descriptor?.value;
    descriptor.resolve = (_: any) => {
        return resolverHandler.call(_);
    };
    object.constructor.ownFields.set(property, descriptor);
};

export const recordField = (
    type: any
) => (
    object: any,
    property: any,
    descriptor: any
) => {
    field(type)(object, property, descriptor);
    object.constructor.ownRecordFields.set(property, descriptor);
};

export interface OperationContext {
    operationType: string;
    target: any;
    property: string;
    descriptor: any;
}

export interface OperationDecoratorParameters {
    parameters?: any;
    returns?: any;
    operationName?: any;
    makeResolver?: any;
}

export const args = (
    argGetterHash: Record<string, (resolverContext: any) => any>
) => (
    target: any,
    propertyName: any,
    index: any
) => {
    
    const propertyDescriptor = Reflect.getOwnPropertyDescriptor(target, propertyName) as any;
    console.log({ propertyDescriptor, muts: target.ownOperations });
    const argGetterEntries = Object.entries(argGetterHash);
    propertyDescriptor.value.args = argGetterEntries.reduce((
        _args: any,
        [name, _get]: any
    ) => Object.assign(_args, {
        [name]: typeof _get === "function" ? _get : _get[getter]
    }), {});
};

export interface OperationSignatureContext {
    operationType: string;
    property: string;
    descriptor: any;
    receiver: any;
}

interface OperationSignature {
    operationName: (context: OperationSignatureContext) => string;
    parameters: (context: OperationSignatureContext) => any;
    returns?: (context: OperationSignatureContext) => any;
}

export const operation = (
    operationType: string
) => ({ 
    operationName,
    parameters, 
    returns = ({ receiver }: any) => [receiver]
}: OperationSignature) => (
    target: any,
    property: string,
    descriptor: any
) => {
    const context = { operationType, property, descriptor };

    if (typeof operationName === "string") {
        descriptor.name = () => operationName;
    } else if (typeof operationName === "function") {
        descriptor.name = ({ receiver }: any) => operationName({ receiver, ...context });
    } else if (typeof operationName === "undefined") {
        descriptor.name = ({ receiver }: any) => `${camelize(receiver.name, false)}${camelize(property)}`;
    }

    descriptor.parameters = ({ receiver }: any) => Object.entries(
        parameters({ receiver, ...context })
    ).reduce((argumentMap: any, [ name, type ]: any[]) => Object.assign(argumentMap, { 
        [name]: {
            type: {
                Symbol: GraphQLID,
                Boolean: GraphQLBoolean,
                Number: GraphQLFloat,
                BigInt: GraphQLInt,
                String: GraphQLString
            }[type.name as string]
        }
    }), {});

    descriptor.returns = ({ receiver }: any) => {
        const contextualizedReturns = returns({ receiver, ...context });
        console.log({contextualizedReturns});
        console.log({ receiver })
        if (contextualizedReturns === Boolean) {
            return GraphQLBoolean;
        } 
        if (Array.isArray(contextualizedReturns)) {
            return receiver?.arrayType;
        }
        return receiver.type;
    };

    descriptor.liftResolver = ({
        receiver,
        method,
    }: {
        receiver: any;
        method: any;
    }) => (
        root: any,
        args: any,
        context: any,
        info: any
    ) => {
        const argEntries = Object.entries(descriptor.value.args);
        const methodArgs = argEntries.reduce((
            _args: any,
            [name, _get]: any
        ) => Object.assign(_args, {
            [name]: _get({ root, args, context, info })
        }), {});
        return method.call(receiver, methodArgs);
    }

    const keyForOwnOperations = `own${camelize(pluralize(operationType))}`
    target[keyForOwnOperations].set(property, descriptor);
};

export const query = operation("query");
export const mutation = operation("mutation");
export const subscription = operation("subscription");

export interface BaseConstructor {
    fields?: Map<string, any>;
    queries?: Map<string, any>;
    mutations?: Map<string, any>;
    subscriptions?: Map<string, any>;
}

export class BaseType {

    static ownFields = new Map();
    static ownQueries = new Map();
    static ownMutations = new Map();
    static ownSubscriptions = new Map();
    static _type: any = null;
    static _arrayType: any = null;

    static get type() {
        if (!this._type) {
            this._type = new GraphQLObjectType({
                name: this.name,
                fields: [...this.fields].reduce((fields, [ fieldName, descriptor ]) => {
                    return Object.assign(fields, {
                        [fieldName]: { type: descriptor.type }
                    });
                }, {}) as unknown as Thunk<GraphQLFieldConfigMap<any, any>>
            });
        }
        return this._type;
    }

    static get arrayType() {
        if (!this._arrayType) {
            this._arrayType = new GraphQLList(this.type)
        }
        return this._arrayType;
    }

    static get fields() {
        if (this === BaseType) {
            return this.ownFields;
        }
        return new Map([
            ...((Reflect.getPrototypeOf(this) as BaseConstructor)?.fields ?? []),
            ...this.ownFields
        ]);
    }

    static get queries() {
        if (this === BaseType) {
            return this.ownQueries;
        }
        return new Map([
            ...((Reflect.getPrototypeOf(this) as BaseConstructor)?.queries ?? []),
            ...this.ownQueries
        ]);
    }

    static get mutations() {
        if (this === BaseType) {
            return this.ownMutations;
        }
        return new Map([
            ...((Reflect.getPrototypeOf(this) as BaseConstructor)?.mutations ?? []),
            ...this.ownMutations
        ]);
    }

    static get subscriptions() {
        if (this === BaseType) {
            return this.ownSubscriptions;
        }
        return new Map([
            ...((Reflect.getPrototypeOf(this) as BaseConstructor)?.subscriptions ?? []),
            ...this.ownSubscriptions
        ]);
    }

    static liftResolver({ liftResolver, value }: any) {
        return liftResolver(value);
    }

    //static createResolvers() {
    //    for (const operationName in ["Query", "Mutation", this.name]) {
    //        const returnType 
    //        const resolversFromEntries = (resolvers, [ propertyKey, descriptor ]) => {
    //            const operationName = descriptor.name({ reciever: this });
    //            const resolver = schemaComposer.createResolver({
    //                type: descriptor.resolverConfig.type,
    //                args: descriptor.resolverConfig.args({ receiver: this }),
    //                resolve: this.liftResolver(descriptor)
    //            })
    //            return { ...resolvers, [operationName]: resolver };
    //        };
    //        const resolvers = this.resolverConfigs.entries().reduce(resolversFromEntries, {});
    //        schemaComposer[operationType].addFields(resolvers);
    //    }
    //}

    constructor() { }

}

