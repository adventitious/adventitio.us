export const flip = (a, b) => (b, a);
export const arrow = (f, g) => (x) => g(f(x));
export const compose = (f, g) => (x) => f(g(x));
export const short = (f, g) => (x) => {
    const s = f(x);
    return s ? s : g(x);
};

export const pipeline = ([...ops]) => ops.reduce(arrow);
export const switch = ([...ops]) => ops.reduce(short);

