import { serve } from "http/server";
import { serveFile } from "http/file-server";
import { posix } from "path";

import React, { useState } from "react";
import ReactDOMServer from "react-dom/server";
import { StaticRouter } from "react-router-dom/server";
//import * as esbuild from 'https://deno.land/x/esbuild@v0.14.27/mod.js'
import { join, dirname } from "https://deno.land/std/path/mod.ts";
import {
    ServerStyleSheet,
} from "styled-components";
import {
    DEFAULT_THEME,
    ThemeProvider    
} from "@zendeskgarden/react-theming";

//import { initialize, login } from "../bindings/bindings.ts";
import { serveRequestIp } from "../net/ip.ts";
import { client as postgres } from "../db/postgres.ts";
import { App } from "../web/components/App.tsx";

import { build as buildEsm } from "https://deno.land/x/esbuild/mod.js";
import svg from 'https://cdn.esm.sh/esbuild-plugin-svg';
import { denoPlugin } from "https://deno.land/x/esbuild_deno_loader@0.4.1/mod.ts";
//import httpFetch from 'https://deno.land/x/esbuild_plugin_http_fetch@v1.0.2/index.ts'
import { pathToRegexp } from "https://raw.githubusercontent.com/pillarjs/path-to-regexp/master/src/index.ts";

import {
    httpImports
} from "https://deno.land/x/esbuild_plugin_http_imports/index.ts";
import {
    load as loadImportMap,
    plugin as importMapPlugin
} from "https://esm.sh/esbuild-plugin-import-map";

import { resetIdCounter } from "@zendeskgarden/react-dropdowns";
import { 
    GraphQLProvider,
    GraphQLClient,
    SchemaLink, 
    serveGraphQLApi,
    schema
} from "../api/index.ts";

Deno.env.set("HOME", Deno.env.get("DENO_DIR"));

const { renderToString } = ReactDOMServer;

const port = 9000;


//initialize(); // Initializes Rust runtime
//initialize(); // Initializes Rust runtime

//const emitP = Deno.emit( "./web/index.tsx", {
//    bundle: "module",
//    compilerOptions: {
//        lib: ["dom", "esnext"]
//    },
//    importMapPath: "./import_map.json"
//});

const importMap: Record<string, string> = {
    "delay": "https://cdn.skypack.dev/delay",
    "gql": "https://cdn.skypack.dev/gql",
    "graphql": "https://cdn.skypack.dev/graphql",
    "graphql-tag": "https://cdn.skypack.dev/graphql-tag",
    "react": "https://cdn.skypack.dev/react",
    //"https://cdn.esm.sh/v74/react@17.0.2/deno/react.js": "https://esm.sh/react@18.0.0?target=esnext",
    "react-dom": "https://cdn.skypack.dev/react-dom",
    "react-router-dom": "https://cdn.skypack.dev/react-router-dom@6.3.0",
    "@zendeskgarden/react-forms": "https://cdn.skypack.dev/@zendeskgarden/react-forms",
    "@zendeskgarden/react-tables": "https://cdn.skypack.dev/@zendeskgarden/react-tables",
    "@zendeskgarden/react-breadcrumbs": "https://cdn.skypack.dev/@zendeskgarden/react-breadcrumbs",
    "@zendeskgarden/react-buttons": "https://cdn.skypack.dev/@zendeskgarden/react-buttons",
    "@zendeskgarden/react-theming": "https://cdn.skypack.dev/@zendeskgarden/react-theming",
    "@zendeskgarden/react-loaders": "https://cdn.skypack.dev/@zendeskgarden/react-loaders",
    "@zendeskgarden/react-typography": "https://cdn.skypack.dev/@zendeskgarden/react-typography@8.49.2",
    "@zendeskgarden/react-notifications": "https://cdn.skypack.dev/@zendeskgarden/react-notifications",
    "@zendeskgarden/react-dropdowns": "https://cdn.skypack.dev/@zendeskgarden/react-dropdowns",
    "@zendeskgarden/react-theming": "https://cdn.skypack.dev/@zendeskgarden/react-theming",
    "@primer/octicons-react": "https://cdn.skypack.dev/@primer/octicons-react",
    "matrix-js-sdk": "https://cdn.esm.sh/matrix-js-sdk",
    "inflected": "https://cdn.skypack.dev/inflected",
    "nanoid": "https://cdn.skypack.dev/nanoid",
    "postgres": "https://cdn.skypack.dev/postgres",
};

//loadImportMap(["./import_map.json"]);

await buildEsm({
    entryPoints: [
        "./web/index.tsx"
    ],
    bundle: true,
    loader: {
        ".mjs": "js",
        ".ts": "ts",
        ".tsx": "tsx",
        ".css": "css"
    },
    plugins: [
        svg(),
        {
            name: "import-map",
            setup: (build) => {
                build.onResolve({ filter: /.*?/ }, (args: any) => {
                    const mappedPath = importMap[args.path];
                    if (
                        (mappedPath ?? "").startsWith("http") || 
                        (args.path ?? "").startsWith("http")
                    ) {
                        return {
                            path: mappedPath ?? args.path,
                            external: true
                        };
                    }
                    return {};
                });
            }
        }
    ],
    external: [
        "react",
        "react-dom",
        "react-router-dom",
        "graphql",
        "graphql-tag",
        "@zendeskgarden/react-theming",
        "@zendeskgarden/react-forms",
        "@zendeskgarden/react-buttons",
        "delay"
    ],
    format: "esm",
    outdir: "./dist"
});

        //<script type="importmap" src="import_map.json"></script>
        //<script>
        //    const Deno = {
        //        args: [],
        //        permissions: undefined,
        //        build: {
        //            arch: "x86_64"
        //        }
        //    };
        //</script>
const indexTemplate = (
    content: string,
    styles: string
): string => `<!doctype html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin />
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Spartan:wght@200;400;700&&family=Fira+Mono:wght@500&display=swap" />
        <link rel="stylesheet" href="/index.css" />
        <link rel="stylesheet" href="/style.css" />
        ${styles}
    </head>
    <body>
        <div id="root">${content}</div>
        <script type="importmap">${JSON.stringify({
            imports: importMap
        })}</script>
        <script src="/index.js" type="module"></script>
    </body>
</html>
`;

export const serveWebApp = async (
    request: Request,
    pathname: string
): Promise<Response> => {

    const gqlClient = new GraphQLClient({
        link: new SchemaLink({ schema }),
    });
    const renderPromises = new Map();
    let sheet = new ServerStyleSheet() as any;

    let rendered = "";
    let renderDepth = 10;
    let renderTimeout = false;

    while (renderDepth-- && !renderTimeout) {
        console.log([...renderPromises.values()]);
        resetIdCounter();
        const partialRender = renderToString(sheet.collectStyles(
            <ThemeProvider theme={{
                ...DEFAULT_THEME
            }}>
                <GraphQLProvider
                    client={gqlClient}
                    renderPromises={renderPromises}
                >
                    <StaticRouter location={pathname}>
                        <App />
                    </StaticRouter>
                </GraphQLProvider>
            </ThemeProvider>
        ));
        console.log({partialRender});
        console.log([...renderPromises.values()]);
        if ([...renderPromises.values()].every((promise: Promise<any>) => {
            return (promise as unknown as { done: boolean; }).done
        })) {
            rendered = partialRender;
            break;
        };
        await Promise.all([...renderPromises.values()]); 
        for (const [ key, renderPromise ] of renderPromises.entries()) {
            renderPromise.done = true;
            renderPromise.result = await renderPromise;
        }
    }

    console.log(rendered);
    const indexPage = indexTemplate(
        rendered as string,
        sheet.getStyleTags() as string
    );

    console.log(indexPage);

    return new Response(indexPage, {
        status: 200,
        headers: {
            "content-type": "text/html"
        }
    });
}

const serveApi = (request: Request): Response => {
    return new Response(JSON.stringify({
        message: "Hello World"
    }), {
        status: 200
    });
};

//const serveWebAppBundle = async (request: Request): Promise<Response> => {
//    console.log("serving webapp.bundle");
//    console.log((await emitP).files["deno:///bundle.js"]);
//    return new Response((await emitP).files["deno:///bundle.js"], {
//        status: 200,
//        headers: {
//            "content-type": "application/javascript"
//        }
//    });
//};

const serveStaticFile = async (
    request: Request,
    pathname: string
): Promise<Response> => { 
    for (const staticDir of [
        "./public",
        "./dist"
    ]) {
        const path = posix.join(staticDir, posix.normalize(decodeURI(pathname)));
        try {
            const fileInfo = await Deno.stat(path);
            if (fileInfo.isDirectory) {
                return;
            }
            if (fileInfo.isFile) {
                return await serveFile(request, path);
            }
        } catch (err) {
            continue;
        }
        console.log(path);
    }
};

const accessToken = "i-have-permission-to-send-you-this";
const MATRIX_CREATE_ROOM_URL = `http://${Deno.env.get("MATRIX_BASE_URL")}:8008/_matrix/client/v3/createRoom`;

await postgres.connect();

await serve(async (request, connInfo): Promise<Response> => {
    const { pathname } = new URL(request.url);
    console.log(pathname);
    const routes = [
        ["/api/checkIp", () => serveRequestIp(request, connInfo)],
        ["/graphql", () => {
            console.log("gqlhandle");
            return serveGraphQLApi(request);
        }],
        ["/rooms/:room_alias", async (match) => {
            const [ , room_alias ] = match;
            const localPart = decodeURIComponent(room_alias).split(":")[0].substring(1);
            const createRoomUri = `${MATRIX_CREATE_ROOM_URL}?access_token=${accessToken}`;
            console.log(MATRIX_CREATE_ROOM_URL);
            console.log(createRoomUri);
            const createRoomResponse = await fetch(createRoomUri, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({
                    room_alias_name: localPart,
                    preset: "public_chat"
                })
            });
            const createdRoom = await createRoomResponse.text();
            return new Response("{}");
        }],
        ["/(.*)", () => serveStaticFile(request, pathname)],
        ["/(.*)", () => serveWebApp(request, pathname)]
    ]

    for (const [ path, handler ] of routes) {
        const regexp = pathToRegexp(path);
        const match = regexp.exec(pathname);
        if (!match) {
            continue;
        }
        const response = await handler(match);
        if (typeof response === "undefined") {
            continue;
        }
        return response;
    }

}, { port });

addEventListener("unload", () => {
    console.log("DOING UNLOAD");
});

