
# Run this mutual aid platform locally for development or testing

The recommended wy to run this is on a linux machine using docker. It should work on WSL and OSX too.

## Pull source code

- Set up SSH keys for GitLab and GitHub
    - check `~/.ssh` to see if you already have a keypair created
    - If there are no keys, create a pair using `ssh-keygen`
    - Visit your user's SSH key settings on GitLab and GitHub and add the public key from your local machine to the keys on GitLab and GitHub.
- Clone this repository
    - `$ git clone git@gitlab.com:adventitious/adventitio.us.git --recurse-submodules`
    - If the repository was already clonde, without the `--recurse-submodules`, pull the submoudles in a seperate command:
        - `$ git submodule update --init`

## Add configuration and secret files

- Copy and tweak the default `dendrite.yml` from the dendrite repo, or ask one of the other developers for a `dendrite.yml`

- Create certs for dendrite
    - `$ cd dendrite`
    - `$ ./build.sh`
    - `$ go run github.com/matrix-org/dendrite/cmd/generate-keys --private-key=../config/matrix_key.pem --tls-cert=../config/server.crt --tls-key=../config/server.key`
    - Check in the `/config` dir to ensure the certificates were created.
- Create SSL certs
    - Should be in `PEM` format, place them in the `ssl` directory as `certificate.pem` for the cert and the key as `private.pem`, otherwise modify the `docker-compose.yml` to pull them frome somewhere else.


- Install docker and docker-compose
    - Arch Linux `$ sudo pacman -Sy docker docker-compose`
- Run the docker daemon
    - This may not be needed on your system.
    - The system may need to be restared for the damon to load.
    - systemd-based OSes `$ sudo systemctl start docker`
- Launch the platform's services
    - This will automatically build images if they havent been built on the local machine yet.
    - `$ docker-compose up`
    - If the Dockerfile has changed, or files the dockerfiles depends on have change, the images will need to be rebuilt.
    - `$ docker-compose up --build`


